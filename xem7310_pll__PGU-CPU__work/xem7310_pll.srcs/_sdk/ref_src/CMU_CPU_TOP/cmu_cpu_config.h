#ifndef __CMU_CPU_CONFIG_H		/* prevent circular inclusions */
#define __CMU_CPU_CONFIG_H		/* by using protection macros */

#ifdef __cplusplus
extern "C" {
#endif

////

#include "xiomodule_l.h" // low-level driver // for XPAR_IOMODULE_0_IO_BASEADDR
#include "xil_printf.h"

// macro for CMU-CPU board support
#define _CMU_CPU_

////
// offset definition for mcs_io_bridge.v
#define MCS_IO_INST_OFFSET              0x00000000 // for LAN
#define MCS_IO_INST_OFFSET_CMU          0x00010000 // for CMU
#define ADRS_BASE           XPAR_IOMODULE_0_IO_BASEADDR + MCS_IO_INST_OFFSET    
#define ADRS_BASE_CMU       XPAR_IOMODULE_0_IO_BASEADDR + MCS_IO_INST_OFFSET_CMU
//
//
#define ADRS_PORT_WI_00         ADRS_BASE     + 0x00000000 // output wire [31:0]
#define ADRS_PORT_WI_01         ADRS_BASE     + 0x00000010 // output wire [31:0]
#define ADRS_PORT_WI_02         ADRS_BASE     + 0x00000020 // output wire [31:0]
#define ADRS_PORT_WI_03         ADRS_BASE     + 0x00000030 // output wire [31:0]
#define ADRS_PORT_WI_04         ADRS_BASE     + 0x00000040 // output wire [31:0]
#define ADRS_PORT_WI_05         ADRS_BASE     + 0x00000050 // output wire [31:0]
#define ADRS_PORT_WI_06         ADRS_BASE     + 0x00000060 // output wire [31:0]
#define ADRS_PORT_WI_07         ADRS_BASE     + 0x00000070 // output wire [31:0]
#define ADRS_PORT_WI_08         ADRS_BASE     + 0x00000080 // output wire [31:0]
#define ADRS_PORT_WI_09         ADRS_BASE     + 0x00000090 // output wire [31:0]
#define ADRS_PORT_WI_0A         ADRS_BASE     + 0x000000A0 // output wire [31:0]
#define ADRS_PORT_WI_0B         ADRS_BASE     + 0x000000B0 // output wire [31:0]
#define ADRS_PORT_WI_0C         ADRS_BASE     + 0x000000C0 // output wire [31:0]
#define ADRS_PORT_WI_0D         ADRS_BASE     + 0x000000D0 // output wire [31:0]
#define ADRS_PORT_WI_0E         ADRS_BASE     + 0x000000E0 // output wire [31:0]
#define ADRS_PORT_WI_0F         ADRS_BASE     + 0x000000F0 // output wire [31:0]
#define ADRS_PORT_WI_10         ADRS_BASE     + 0x00000100 // output wire [31:0]
#define ADRS_PORT_WI_11         ADRS_BASE     + 0x00000110 // output wire [31:0]
#define ADRS_PORT_WI_12         ADRS_BASE     + 0x00000120 // output wire [31:0]
#define ADRS_PORT_WI_13         ADRS_BASE     + 0x00000130 // output wire [31:0]
#define ADRS_PORT_WI_14         ADRS_BASE     + 0x00000140 // output wire [31:0]
#define ADRS_PORT_WI_15         ADRS_BASE     + 0x00000150 // output wire [31:0]
#define ADRS_PORT_WI_16         ADRS_BASE     + 0x00000160 // output wire [31:0]
#define ADRS_PORT_WI_17         ADRS_BASE     + 0x00000170 // output wire [31:0]
#define ADRS_PORT_WI_18         ADRS_BASE     + 0x00000180 // output wire [31:0]
#define ADRS_PORT_WI_19         ADRS_BASE     + 0x00000190 // output wire [31:0]
#define ADRS_PORT_WI_1A         ADRS_BASE     + 0x000001A0 // output wire [31:0]
#define ADRS_PORT_WI_1B         ADRS_BASE     + 0x000001B0 // output wire [31:0]
#define ADRS_PORT_WI_1C         ADRS_BASE     + 0x000001C0 // output wire [31:0]
#define ADRS_PORT_WI_1D         ADRS_BASE     + 0x000001D0 // output wire [31:0]
#define ADRS_PORT_WI_1E         ADRS_BASE     + 0x000001E0 // output wire [31:0]
#define ADRS_PORT_WI_1F         ADRS_BASE     + 0x000001F0 // output wire [31:0]
//
#define ADRS_PORT_WO_20         ADRS_BASE     + 0x00000200 // input wire [31:0]
#define ADRS_PORT_WO_21         ADRS_BASE     + 0x00000210 // input wire [31:0]
//
#define ADRS_PORT_TI_40         ADRS_BASE     + 0x00000400 // input wire, output wire [31:0],
#define ADRS_PORT_TI_41         ADRS_BASE     + 0x00000410 // input wire, output wire [31:0],
//
#define ADRS_PORT_TO_60         ADRS_BASE     + 0x00000600 // input wire, input wire [31:0],
#define ADRS_PORT_TO_61         ADRS_BASE     + 0x00000610 // input wire, input wire [31:0],
//
#define ADRS_PORT_PI_80         ADRS_BASE     + 0x00000800 // output wire, output wire [31:0],
#define ADRS_PORT_PI_81         ADRS_BASE     + 0x00000810 // output wire, output wire [31:0],
//
#define ADRS_PORT_PO_A0         ADRS_BASE     + 0x00000A00 // output wire, input wire [31:0],
#define ADRS_PORT_PO_A1         ADRS_BASE     + 0x00000A10 // output wire, input wire [31:0],
//
#define ADRS_FPGA_IMAGE         ADRS_BASE     + 0x00000F00 // image id
#define ADRS_TEST_REG           ADRS_BASE     + 0x00000F04 // test reg 
#define ADRS_MASK_WI            ADRS_BASE     + 0x00000F10 // mask
#define ADRS_MASK_WO            ADRS_BASE     + 0x00000F14 // mask
#define ADRS_MASK_TI            ADRS_BASE     + 0x00000F18 // mask
#define ADRS_MASK_TO            ADRS_BASE     + 0x00000F1C // mask
//
//
#define ADRS_PORT_WI_00_CMU     ADRS_BASE_CMU + 0x00000000 // output wire [31:0]
#define ADRS_PORT_WI_01_CMU     ADRS_BASE_CMU + 0x00000010 // output wire [31:0]
#define ADRS_PORT_WI_02_CMU     ADRS_BASE_CMU + 0x00000020 // output wire [31:0]
#define ADRS_PORT_WI_03_CMU     ADRS_BASE_CMU + 0x00000030 // output wire [31:0]
#define ADRS_PORT_WI_04_CMU     ADRS_BASE_CMU + 0x00000040 // output wire [31:0]
#define ADRS_PORT_WI_05_CMU     ADRS_BASE_CMU + 0x00000050 // output wire [31:0]
#define ADRS_PORT_WI_06_CMU     ADRS_BASE_CMU + 0x00000060 // output wire [31:0]
#define ADRS_PORT_WI_07_CMU     ADRS_BASE_CMU + 0x00000070 // output wire [31:0]
#define ADRS_PORT_WI_08_CMU     ADRS_BASE_CMU + 0x00000080 // output wire [31:0]
#define ADRS_PORT_WI_09_CMU     ADRS_BASE_CMU + 0x00000090 // output wire [31:0]
#define ADRS_PORT_WI_0A_CMU     ADRS_BASE_CMU + 0x000000A0 // output wire [31:0]
#define ADRS_PORT_WI_0B_CMU     ADRS_BASE_CMU + 0x000000B0 // output wire [31:0]
#define ADRS_PORT_WI_0C_CMU     ADRS_BASE_CMU + 0x000000C0 // output wire [31:0]
#define ADRS_PORT_WI_0D_CMU     ADRS_BASE_CMU + 0x000000D0 // output wire [31:0]
#define ADRS_PORT_WI_0E_CMU     ADRS_BASE_CMU + 0x000000E0 // output wire [31:0]
#define ADRS_PORT_WI_0F_CMU     ADRS_BASE_CMU + 0x000000F0 // output wire [31:0]

#define ADRS_PORT_WI_10_CMU     ADRS_BASE_CMU + 0x00000100 // output wire [31:0]
#define ADRS_PORT_WI_11_CMU     ADRS_BASE_CMU + 0x00000110 // output wire [31:0]
#define ADRS_PORT_WI_12_CMU     ADRS_BASE_CMU + 0x00000120 // output wire [31:0]
#define ADRS_PORT_WI_13_CMU     ADRS_BASE_CMU + 0x00000130 // output wire [31:0]
#define ADRS_PORT_WI_14_CMU     ADRS_BASE_CMU + 0x00000140 // output wire [31:0]
#define ADRS_PORT_WI_15_CMU     ADRS_BASE_CMU + 0x00000150 // output wire [31:0]
#define ADRS_PORT_WI_16_CMU     ADRS_BASE_CMU + 0x00000160 // output wire [31:0]
#define ADRS_PORT_WI_17_CMU     ADRS_BASE_CMU + 0x00000170 // output wire [31:0]
#define ADRS_PORT_WI_18_CMU     ADRS_BASE_CMU + 0x00000180 // output wire [31:0]
#define ADRS_PORT_WI_19_CMU     ADRS_BASE_CMU + 0x00000190 // output wire [31:0]
#define ADRS_PORT_WI_1A_CMU     ADRS_BASE_CMU + 0x000001A0 // output wire [31:0]
#define ADRS_PORT_WI_1B_CMU     ADRS_BASE_CMU + 0x000001B0 // output wire [31:0]
#define ADRS_PORT_WI_1C_CMU     ADRS_BASE_CMU + 0x000001C0 // output wire [31:0]
#define ADRS_PORT_WI_1D_CMU     ADRS_BASE_CMU + 0x000001D0 // output wire [31:0]
#define ADRS_PORT_WI_1E_CMU     ADRS_BASE_CMU + 0x000001E0 // output wire [31:0]
#define ADRS_PORT_WI_1F_CMU     ADRS_BASE_CMU + 0x000001F0 // output wire [31:0]
//
#define ADRS_PORT_WO_20_CMU     ADRS_BASE_CMU + 0x00000200 // input wire [31:0]
#define ADRS_PORT_WO_21_CMU     ADRS_BASE_CMU + 0x00000210 // input wire [31:0]
#define ADRS_PORT_TI_40_CMU     ADRS_BASE_CMU + 0x00000400 // input wire, output wire [31:0],
#define ADRS_PORT_TI_41_CMU     ADRS_BASE_CMU + 0x00000410 // input wire, output wire [31:0],
#define ADRS_PORT_TO_60_CMU     ADRS_BASE_CMU + 0x00000600 // input wire, input wire [31:0],
#define ADRS_PORT_TO_61_CMU     ADRS_BASE_CMU + 0x00000610 // input wire, input wire [31:0],
#define ADRS_PORT_PI_80_CMU     ADRS_BASE_CMU + 0x00000800 // output wire, output wire [31:0],
#define ADRS_PORT_PI_81_CMU     ADRS_BASE_CMU + 0x00000810 // output wire, output wire [31:0],
#define ADRS_PORT_PI_8A_CMU     ADRS_BASE_CMU + 0x000008A0 // output wire, output wire [31:0],
#define ADRS_PORT_PO_A0_CMU     ADRS_BASE_CMU + 0x00000A00 // output wire, input wire [31:0],
#define ADRS_PORT_PO_A1_CMU     ADRS_BASE_CMU + 0x00000A10 // output wire, input wire [31:0],
#define ADRS_PORT_PO_AA_CMU     ADRS_BASE_CMU + 0x00000AA0 // output wire, input wire [31:0],
//
#define ADRS_FPGA_IMAGE_CMU     ADRS_BASE_CMU + 0x00000F00 // image id
#define ADRS_TEST_REG___CMU     ADRS_BASE_CMU + 0x00000F04 // test reg 
#define ADRS_MASK_WI____CMU     ADRS_BASE_CMU + 0x00000F10 // mask
#define ADRS_MASK_WO____CMU     ADRS_BASE_CMU + 0x00000F14 // mask
#define ADRS_MASK_TI____CMU     ADRS_BASE_CMU + 0x00000F18 // mask
#define ADRS_MASK_TO____CMU     ADRS_BASE_CMU + 0x00000F1C // mask
//
////

////
// CMU end points 
//
#define EP_ADRS__board_name           "CMU-CPU-F5500"
#define EP_ADRS__ver                  0xF3190306
#define EP_ADRS__bit_filename         "xem7310__cmu_cpu__top__F3_19_0306.bit"
//
#define MASK_ALL                      0xFFFFFFFF
//
#define CMU_PAR__OFF                  0x00000000
#define CMU_PAR__ON                   0x00000001
#define CMU_PAR__RESET                0x00000002
#define CMU_PAR__GN_OFF               0x00000010
#define CMU_PAR__GN_1X                0x00000011
#define CMU_PAR__GN_10X               0x00000012
#define CMU_PAR__GN_100X              0x00000013
#define CMU_PAR__BW_OFF               0x00000020
#define CMU_PAR__BW_120K              0x00000021
#define CMU_PAR__BW_1M2               0x00000022
#define CMU_PAR__BW_12M               0x00000023
#define CMU_PAR__BW_120M              0x00000024
//
//wire-in
#define EP_ADRS__SW_BUILD_ID          0x00
#define EP_ADRS__TEST_CON             0x01
#define EP_ADRS__TEST_CC_DIN          0x02
#define EP_ADRS__wi03                 0x03
#define EP_ADRS__DAC_TEST_IN          0x04
#define EP_ADRS__DWAVE_DIN_BY_TRIG    0x05
#define EP_ADRS__DWAVE_CON            0x06
#define EP_ADRS__SPO_CON              0x07
#define EP_ADRS__SPO_DIN_B0_L         0x08
#define EP_ADRS__SPO_DIN_B0_H         0x09
#define EP_ADRS__SPO_DIN_B1_L         0x0A
#define EP_ADRS__SPO_DIN_B1_H         0x0B
#define EP_ADRS__SPO_DIN_B2_L         0x0C
#define EP_ADRS__SPO_DIN_B2_H         0x0D
#define EP_ADRS__SPO_DIN_B3_L         0x0E
#define EP_ADRS__SPO_DIN_B3_H         0x0F
#define EP_ADRS__DAC_A2A3_CON         0x10
#define EP_ADRS__DAC_BIAS_CON         0x11
#define EP_ADRS__wi12                 0x12
#define EP_ADRS__wi13                 0x13
#define EP_ADRS__DAC_A2A3_DIN21       0x14
#define EP_ADRS__DAC_A2A3_DIN43       0x15
#define EP_ADRS__DAC_BIAS_DIN21       0x16
#define EP_ADRS__DAC_BIAS_DIN43       0x17
#define EP_ADRS__ADC_HS_WI            0x18
#define EP_ADRS__wi19                 0x19
#define EP_ADRS__wi1A                 0x1A
#define EP_ADRS__wi1B                 0x1B
#define EP_ADRS__wi1C                 0x1C
#define EP_ADRS__ADC_HS_UPD_SMP       0x1D
#define EP_ADRS__ADC_HS_SMP_PRD       0x1E
#define EP_ADRS__ADC_HS_DLY_TAP_OPT   0x1F
//wire-out           
#define EP_ADRS__FPGA_IMAGE_ID        0x20
#define EP_ADRS__TEST_OUT             0x21
#define EP_ADRS__TEST_CC_MON          0x22
#define EP_ADRS__DWAVE_BASE_FREQ      0x23
#define EP_ADRS__DAC_TEST_OUT         0x24
#define EP_ADRS__DWAVE_DOUT_BY_TRIG   0x25
#define EP_ADRS__DWAVE_FLAG           0x26
#define EP_ADRS__SPO_FLAG             0x27
#define EP_ADRS__SPO_MON_B0_L         0x28
#define EP_ADRS__SPO_MON_B0_H         0x29
#define EP_ADRS__SPO_MON_B1_L         0x2A
#define EP_ADRS__SPO_MON_B1_H         0x2B
#define EP_ADRS__SPO_MON_B2_L         0x2C
#define EP_ADRS__SPO_MON_B2_H         0x2D
#define EP_ADRS__SPO_MON_B3_L         0x2E
#define EP_ADRS__SPO_MON_B3_H         0x2F
#define EP_ADRS__DAC_A2A3_FLAG        0x30
#define EP_ADRS__DAC_BIAS_FLAG        0x31
#define EP_ADRS__DAC_TEST_RB1         0x32
#define EP_ADRS__DAC_TEST_RB2         0x33
#define EP_ADRS__DAC_A2A3_RB21        0x34
#define EP_ADRS__DAC_A2A3_RB43        0x35
#define EP_ADRS__DAC_BIAS_RB21        0x36
#define EP_ADRS__DAC_BIAS_RB43        0x37
#define EP_ADRS__ADC_HS_WO            0x38
#define EP_ADRS__ADC_BASE_FREQ        0x39
#define EP_ADRS__XADC_TEMP            0x3A
#define EP_ADRS__XADC_VOLT            0x3B
#define EP_ADRS__ADC_HS_DOUT0         0x3C
#define EP_ADRS__ADC_HS_DOUT1         0x3D
#define EP_ADRS__ADC_HS_DOUT2         0x3E
#define EP_ADRS__ADC_HS_DOUT3         0x3F
//trig-in            
#define EP_ADRS__TEST_TI              0x40
#define EP_ADRS__TEST_TI_HS           0x41 //##$$ 2019/3/5, 2018/12/8 
#define EP_ADRS__DWAVE_TI             0x46
#define EP_ADRS__DAC_BIAS_TI          0x50
#define EP_ADRS__DAC_A2A3_TI          0x51
#define EP_ADRS__ADC_HS_TI            0x58
//trig-out                           
#define EP_ADRS__TEST_TO              0x60
#define EP_ADRS__DAC_BIAS_TO          0x70
#define EP_ADRS__DAC_A2A3_TO          0x71
#define EP_ADRS__ADC_HS_TO            0x78
//pipe-out                           
#define EP_ADRS__ADC_HS_DOUT0_PO      0xBC
#define EP_ADRS__ADC_HS_DOUT1_PO      0xBD
#define EP_ADRS__ADC_HS_DOUT2_PO      0xBE
#define EP_ADRS__ADC_HS_DOUT3_PO      0xBF
//
////

#ifdef __cplusplus
}
#endif

#endif /* end of protection macro */
