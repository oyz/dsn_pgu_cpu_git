
##== pulse loopback ==##
#set_property MARK_DEBUG true [get_nets r_cnt__pulse_loopback*]
#set_property MARK_DEBUG true [get_nets w_enable__pulse_loopback*]
#set_property MARK_DEBUG true [get_nets w_pulse_loopback_in]
#set_property MARK_DEBUG true [get_nets w_pulse_out]
#set_property MARK_DEBUG true [get_nets w_reset__pulse_loopback_cnt*]
#set_property MARK_DEBUG true [get_nets w_trig__pulse_shot]

##== adc ==##
#set_property MARK_DEBUG true [get_nets {control_hsadc_quad_inst/state[*]}]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_clk_adc]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_cnv_adc]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_clk_reset]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_io_reset]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_pin_dlln_frc_low]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_pttn_cnt_up_en]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/w_delay_locked]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/w_io_reset]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/w_valid_serdes]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/check_cnt_cnv]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/w_cnv_adc_en]
#
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/w_adc_done_init]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/check_cnt_cnv__init]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_init_trig]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_init_busy]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_init_done]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/check_cnt_cnv__init__adc1]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/check_cnt_cnv__init__adc2]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/check_cnt_cnv__init__adc3]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/check_cnt_cnv__init__adc0]
#set_property MARK_DEBUG true [get_nets {control_hsadc_quad_inst/cnt_wr_fifo[0]__0[*]}]
#set_property MARK_DEBUG true [get_nets {control_hsadc_quad_inst/cnt_wr_fifo[1]__0[*]}]
#set_property MARK_DEBUG true [get_nets {control_hsadc_quad_inst/cnt_wr_fifo[2]__0[*]}]
#set_property MARK_DEBUG true [get_nets {control_hsadc_quad_inst/cnt_wr_fifo[3]__0[*]}]
#set_property MARK_DEBUG true [get_nets {control_hsadc_quad_inst/cnt_cnv[*]}]



##== TEMP SENSOR ==##
#
#set_property MARK_DEBUG true [get_nets r_temp_sig*]
#set_property MARK_DEBUG true [get_nets r_toggle_temp_sig*]


##== SPIO ==##
set_property MARK_DEBUG true [get_nets SPIO0_CS]
set_property MARK_DEBUG true [get_nets SPIO1_CS]
set_property MARK_DEBUG true [get_nets SPIOx_MOSI]
set_property MARK_DEBUG true [get_nets SPIOx_MISO]
set_property MARK_DEBUG true [get_nets SPIOx_SCLK]
set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/state[*]}]
##set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/r_fbit_index[*]}]
##set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/r_shift_send_w32[*]}]
##set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/r_shift_recv_w16[*]}]
##set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/r_R_W_bar]
set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/r_rd_DA[*]}]
set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/r_rd_DB[*]}]
set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/r_trig_SPI_frame]
set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/r_done_SPI_frame]
##set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/r_LNG_RSTn]
##set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/r_done_LNG_reset]

##== CLKD ==##
set_property MARK_DEBUG true [get_nets CLKD_CS_B]
set_property MARK_DEBUG true [get_nets CLKD_SCLK]
set_property MARK_DEBUG true [get_nets CLKD_SDIO]
#set_property MARK_DEBUG true [get_nets CLKD_SDIO_rd]
set_property MARK_DEBUG true [get_nets CLKD_SDO]
#set_property MARK_DEBUG true [get_nets CLKD_RST_B]
#set_property MARK_DEBUG true [get_nets CLKD_REFM]
#set_property MARK_DEBUG true [get_nets CLKD_LD]
#set_property MARK_DEBUG true [get_nets CLKD_STAT]
set_property MARK_DEBUG true [get_nets {master_spi_ad9516_inst/state[*]}]
#set_property MARK_DEBUG true [get_nets master_spi_ad9516_inst/r_clken_div]
#set_property MARK_DEBUG true [get_nets {master_spi_ad9516_inst/r_fbit_index[*]}]
#set_property MARK_DEBUG true [get_nets {master_spi_ad9516_inst/r_shift_send_w24[*]}]
#set_property MARK_DEBUG true [get_nets {master_spi_ad9516_inst/r_shift_recv_w8[*]}]
#set_property MARK_DEBUG true [get_nets master_spi_ad9516_inst/r_R_W_bar]
set_property MARK_DEBUG true [get_nets {master_spi_ad9516_inst/r_rd_D[*]}]
set_property MARK_DEBUG true [get_nets master_spi_ad9516_inst/r_trig_SPI_frame]
set_property MARK_DEBUG true [get_nets master_spi_ad9516_inst/r_done_SPI_frame]
#set_property MARK_DEBUG true [get_nets master_spi_ad9516_inst/i_trig_SPI_frame]
#set_property MARK_DEBUG true [get_nets master_spi_ad9516_inst/r_LNG_RSTn]
#set_property MARK_DEBUG true [get_nets master_spi_ad9516_inst/i_trig_LNG_reset]
#set_property MARK_DEBUG true [get_nets clk_dac_locked]
#set_property MARK_DEBUG true [get_nets w_CLKD_WO[*]]
#set_property MARK_DEBUG true [get_nets w_CLKD_WI[*]]
#set_property MARK_DEBUG true [get_nets w_CLKD_TI[*]]


##== DACX ==##
set_property MARK_DEBUG true [get_nets DAC0_CS]
set_property MARK_DEBUG true [get_nets DAC1_CS]
set_property MARK_DEBUG true [get_nets DACx_SCLK]
set_property MARK_DEBUG true [get_nets DACx_SDIO]
set_property MARK_DEBUG true [get_nets DACx_SDO]
set_property MARK_DEBUG true [get_nets DACx_RST_B]
set_property MARK_DEBUG true [get_nets {master_spi_ad9783_inst/state[*]}]
##set_property MARK_DEBUG true [get_nets {master_spi_ad9783_inst/r_fbit_index[*]}]
##set_property MARK_DEBUG true [get_nets {master_spi_ad9783_inst/r_shift_send_w16[*]}]
##set_property MARK_DEBUG true [get_nets {master_spi_ad9783_inst/r_shift_recv_w8[*]}]
##set_property MARK_DEBUG true [get_nets master_spi_ad9783_inst/r_R_W_bar]
set_property MARK_DEBUG true [get_nets {master_spi_ad9783_inst/r_rd_D[*]}]
set_property MARK_DEBUG true [get_nets master_spi_ad9783_inst/r_trig_SPI_frame]
set_property MARK_DEBUG true [get_nets master_spi_ad9783_inst/r_done_SPI_frame]
##set_property MARK_DEBUG true [get_nets master_spi_ad9783_inst/r_LNG_RSTn]
##set_property MARK_DEBUG true [get_nets dac0_dco_clk_locked]
##set_property MARK_DEBUG true [get_nets dac1_dco_clk_locked]


##== DAC Pattern Gen ==##
set_property MARK_DEBUG true [get_nets {DAC0_DAT[*]}]
set_property MARK_DEBUG true [get_nets {DAC1_DAT[*]}]
#
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/i_rstn_dac0_dco]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/i_rstn_dac1_dco]
#
set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/o_dac0_active_dco]
set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/o_dac1_active_dco]
set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/o_dac0_active_clk]
set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/o_dac1_active_clk]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/flag_dcs_active_dac0]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/flag_dcs_active_dac1]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/flag_fdcs_active_dac0]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/flag_fdcs_active_dac1]
#
##set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_mls_dacx_data_seq_0[*]}]
##set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_mls_dacx_data_seq_1[*]}]
##set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_mls_dacx_data_seq_2[*]}]
##set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_mls_dacx_data_seq_3[*]}]
##set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_mls_dacx_adrs[*]}]
##set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/i_trig_dacx_ctrl[*]}]
##set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/flag_mls_run]
##set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_mls_dac0[*]}]
##set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_mls_dac1[*]}]
##set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_cnt_adrs_dac0[*]}]
##set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_cnt_adrs_dac1[*]}]
##set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_wire_dacx_data[*]}]
#
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/r_dcs_run]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/flag_dcs_run_dac0]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/flag_dcs_run_dac1]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/flag_dcs_rdy_dac0]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/flag_dcs_rdy_dac1]
#set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_dcs_data_dac0[*]}]
#set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_dcs_data_dac1[*]}]
#set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_dcs_repeat_dac0[*]}]
#set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_dcs_repeat_dac1[*]}]
#set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_dcs_cnt_duration_dac0[*]}]
#set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_dcs_cnt_duration_dac1[*]}]
#set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_dcs_cnt_adrs_dac0[*]}]
#set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_dcs_cnt_adrs_dac1[*]}]
#set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_dcs_cnt_repeat_dac0[*]}]
#set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_dcs_cnt_repeat_dac1[*]}]
#
set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/r_fdcs_run]
set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/flag_fdcs_run_dac0]
set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/flag_fdcs_run_dac1]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/flag_fdcs_rdy_dac0]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/flag_fdcs_rdy_dac1]
set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_fdcs_data_dac0[*]}]
set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_fdcs_data_dac1[*]}]
set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_fdcs_repeat_dac0[*]}]
set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_fdcs_repeat_dac1[*]}]
set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[*]}]
set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[*]}]
set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[*]}]
set_property MARK_DEBUG true [get_nets {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[*]}]
#
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/r_dac0_fifo_rd_en]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/r_dac0_fifo_empty]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/r_dac0_fifo_rst]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/r_dac0_fifo_reload1_rst]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/r_dac0_fifo_reload2_rst]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/r_dac1_fifo_rd_en]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/r_dac1_fifo_empty]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/r_dac1_fifo_rst]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/r_dac1_fifo_reload1_rst]
#set_property MARK_DEBUG true [get_nets dac_pattern_gen_inst/r_dac1_fifo_reload2_rst]
#
#set_property MARK_DEBUG true [get_nets {w_DAC0_DAT_PI[*]}]
#set_property MARK_DEBUG true [get_nets {w_DAC1_DAT_PI[*]}]
#set_property MARK_DEBUG true [get_nets w_DAC0_DAT_PI_WR]
#set_property MARK_DEBUG true [get_nets w_DAC1_DAT_PI_WR]


##== time stamp ==##
#set_property MARK_DEBUG true [get_nets {sub_timestamp_inst/r_global_time_idx[*]}]
set_property MARK_DEBUG true [get_nets {w_TIMESTAMP_WO[*]}]


##== soft CPU (MCS) ==##
#set_property MARK_DEBUG true [get_nets soft_cpu_mcs_inst/Reset]
#set_property MARK_DEBUG true [get_nets IO_addr_strobe]
#set_property MARK_DEBUG true [get_nets {IO_address[*]}]
#set_property MARK_DEBUG true [get_nets {IO_byte_enable[*]}]
#set_property MARK_DEBUG true [get_nets {IO_read_data[*]}]
#set_property MARK_DEBUG true [get_nets IO_read_strobe]
#set_property MARK_DEBUG true [get_nets IO_ready]
#set_property MARK_DEBUG true [get_nets {IO_write_data[*]}]
#set_property MARK_DEBUG true [get_nets IO_write_strobe]
##
### mcs_io_bridge_inst0
#set_property MARK_DEBUG true [get_nets {IO_read_data_0[*]}]
#set_property MARK_DEBUG true [get_nets IO_ready_0]
#set_property MARK_DEBUG true [get_nets IO_ready_ref_0]
### mcs_io_bridge_inst1
#set_property MARK_DEBUG true [get_nets {IO_read_data_1[*]}]
#set_property MARK_DEBUG true [get_nets IO_ready_1]
#set_property MARK_DEBUG true [get_nets IO_ready_ref_1]

##== LAN ==##
#set_property MARK_DEBUG true [get_nets w_trig_LAN_reset]
#set_property MARK_DEBUG true [get_nets w_trig_SPI_frame]
##
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/i_trig_LAN_reset]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/o_done_LAN_reset]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/i_trig_SPI_frame]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/r_done_SPI_frame]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/o_LAN_RSTn]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/r_LAN_SCSn]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/i_LAN_MISO]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/o_LAN_MOSI]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/r_LAN_SCLK]
#set_property MARK_DEBUG true [get_nets {master_spi_wz850_inst/state[*]}]
#set_property MARK_DEBUG true [get_nets {master_spi_wz850_inst/r_sh_buf_data[*]}]
#set_property MARK_DEBUG true [get_nets {master_spi_wz850_inst/r_sh_buf_data_read[*]}]
#set_property MARK_DEBUG true [get_nets {master_spi_wz850_inst/r_sh_buf_adct[*]}]
#set_property MARK_DEBUG true [get_nets {master_spi_wz850_inst/i_frame_num_byte_data[*]}]
#set_property MARK_DEBUG true [get_nets {master_spi_wz850_inst/i_frame_data_wr[*]}]
#set_property MARK_DEBUG true [get_nets {master_spi_wz850_inst/o_frame_data_rd[*]}]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/i_frame_ctrl_rdwr_sel]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/o_frame_done_wr]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/o_frame_done_rd]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/r_frame_adct]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/r_frame_data]


##== LAN-FIFO ==##
#set_property MARK_DEBUG true [get_nets {LAN_fifo_rd_inst/din[*]}]
#set_property MARK_DEBUG true [get_nets {LAN_fifo_rd_inst/dout[*]}]
#set_property MARK_DEBUG true [get_nets LAN_fifo_rd_inst/rd_en]
#set_property MARK_DEBUG true [get_nets LAN_fifo_rd_inst/valid]
#set_property MARK_DEBUG true [get_nets LAN_fifo_rd_inst/wr_en]
#set_property MARK_DEBUG true [get_nets LAN_fifo_rd_inst/wr_ack]
#set_property MARK_DEBUG true [get_nets LAN_fifo_rd_inst/empty]
##
#set_property MARK_DEBUG true [get_nets {LAN_fifo_wr_inst/din[*]}]
#set_property MARK_DEBUG true [get_nets {LAN_fifo_wr_inst/dout[*]}]
#set_property MARK_DEBUG true [get_nets LAN_fifo_wr_inst/rd_en]
#set_property MARK_DEBUG true [get_nets LAN_fifo_wr_inst/valid]
#set_property MARK_DEBUG true [get_nets LAN_fifo_wr_inst/wr_en]
#set_property MARK_DEBUG true [get_nets LAN_fifo_wr_inst/wr_ack]
#set_property MARK_DEBUG true [get_nets LAN_fifo_wr_inst/empty]


##------------------------------------------------------------------------##



#create_debug_core u_ila_0 ila
#set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
#set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
#set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
#set_property C_DATA_DEPTH 4096 [get_debug_cores u_ila_0]
#set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
#set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
#set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
#set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
#set_property port_width 1 [get_debug_ports u_ila_0/clk]
#connect_debug_port u_ila_0/clk [get_nets [list clk_wiz_1_0_inst/inst/clk_out1_400M]]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
#set_property port_width 16 [get_debug_ports u_ila_0/probe0]
#connect_debug_port u_ila_0/probe0 [get_nets [list {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[0]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[1]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[2]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[3]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[4]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[5]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[6]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[7]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[8]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[9]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[10]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[11]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[12]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[13]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[14]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac0[15]}]]
#create_debug_core u_ila_1 ila
#set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_1]
#set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_1]
#set_property C_ADV_TRIGGER false [get_debug_cores u_ila_1]
#set_property C_DATA_DEPTH 4096 [get_debug_cores u_ila_1]
#set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_1]
#set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_1]
#set_property C_TRIGIN_EN false [get_debug_cores u_ila_1]
#set_property C_TRIGOUT_EN false [get_debug_cores u_ila_1]
#set_property port_width 1 [get_debug_ports u_ila_1/clk]
#connect_debug_port u_ila_1/clk [get_nets [list clk_wiz_1_1_inst/inst/clk_out1_400M]]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe0]
#set_property port_width 16 [get_debug_ports u_ila_1/probe0]
#connect_debug_port u_ila_1/probe0 [get_nets [list {dac_pattern_gen_inst/r_fdcs_data_dac1[0]} {dac_pattern_gen_inst/r_fdcs_data_dac1[1]} {dac_pattern_gen_inst/r_fdcs_data_dac1[2]} {dac_pattern_gen_inst/r_fdcs_data_dac1[3]} {dac_pattern_gen_inst/r_fdcs_data_dac1[4]} {dac_pattern_gen_inst/r_fdcs_data_dac1[5]} {dac_pattern_gen_inst/r_fdcs_data_dac1[6]} {dac_pattern_gen_inst/r_fdcs_data_dac1[7]} {dac_pattern_gen_inst/r_fdcs_data_dac1[8]} {dac_pattern_gen_inst/r_fdcs_data_dac1[9]} {dac_pattern_gen_inst/r_fdcs_data_dac1[10]} {dac_pattern_gen_inst/r_fdcs_data_dac1[11]} {dac_pattern_gen_inst/r_fdcs_data_dac1[12]} {dac_pattern_gen_inst/r_fdcs_data_dac1[13]} {dac_pattern_gen_inst/r_fdcs_data_dac1[14]} {dac_pattern_gen_inst/r_fdcs_data_dac1[15]}]]
#create_debug_core u_ila_2 ila
#set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_2]
#set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_2]
#set_property C_ADV_TRIGGER false [get_debug_cores u_ila_2]
#set_property C_DATA_DEPTH 4096 [get_debug_cores u_ila_2]
#set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_2]
#set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_2]
#set_property C_TRIGIN_EN false [get_debug_cores u_ila_2]
#set_property C_TRIGOUT_EN false [get_debug_cores u_ila_2]
#set_property port_width 1 [get_debug_ports u_ila_2/clk]
#connect_debug_port u_ila_2/clk [get_nets [list clk_wiz_0_3_inst/inst/clk_out1_72M]]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe0]
#set_property port_width 32 [get_debug_ports u_ila_2/probe0]
#connect_debug_port u_ila_2/probe0 [get_nets [list {IO_write_data[0]} {IO_write_data[1]} {IO_write_data[2]} {IO_write_data[3]} {IO_write_data[4]} {IO_write_data[5]} {IO_write_data[6]} {IO_write_data[7]} {IO_write_data[8]} {IO_write_data[9]} {IO_write_data[10]} {IO_write_data[11]} {IO_write_data[12]} {IO_write_data[13]} {IO_write_data[14]} {IO_write_data[15]} {IO_write_data[16]} {IO_write_data[17]} {IO_write_data[18]} {IO_write_data[19]} {IO_write_data[20]} {IO_write_data[21]} {IO_write_data[22]} {IO_write_data[23]} {IO_write_data[24]} {IO_write_data[25]} {IO_write_data[26]} {IO_write_data[27]} {IO_write_data[28]} {IO_write_data[29]} {IO_write_data[30]} {IO_write_data[31]}]]
#create_debug_core u_ila_3 ila
#set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_3]
#set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_3]
#set_property C_ADV_TRIGGER false [get_debug_cores u_ila_3]
#set_property C_DATA_DEPTH 4096 [get_debug_cores u_ila_3]
#set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_3]
#set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_3]
#set_property C_TRIGIN_EN false [get_debug_cores u_ila_3]
#set_property C_TRIGOUT_EN false [get_debug_cores u_ila_3]
#set_property port_width 1 [get_debug_ports u_ila_3/clk]
#connect_debug_port u_ila_3/clk [get_nets [list clk_wiz_0_inst/inst/clk_out3_10M]]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe0]
#set_property port_width 16 [get_debug_ports u_ila_3/probe0]
#connect_debug_port u_ila_3/probe0 [get_nets [list {dac_pattern_gen_inst/r_fdcs_repeat_dac1[0]} {dac_pattern_gen_inst/r_fdcs_repeat_dac1[1]} {dac_pattern_gen_inst/r_fdcs_repeat_dac1[2]} {dac_pattern_gen_inst/r_fdcs_repeat_dac1[3]} {dac_pattern_gen_inst/r_fdcs_repeat_dac1[4]} {dac_pattern_gen_inst/r_fdcs_repeat_dac1[5]} {dac_pattern_gen_inst/r_fdcs_repeat_dac1[6]} {dac_pattern_gen_inst/r_fdcs_repeat_dac1[7]} {dac_pattern_gen_inst/r_fdcs_repeat_dac1[8]} {dac_pattern_gen_inst/r_fdcs_repeat_dac1[9]} {dac_pattern_gen_inst/r_fdcs_repeat_dac1[10]} {dac_pattern_gen_inst/r_fdcs_repeat_dac1[11]} {dac_pattern_gen_inst/r_fdcs_repeat_dac1[12]} {dac_pattern_gen_inst/r_fdcs_repeat_dac1[13]} {dac_pattern_gen_inst/r_fdcs_repeat_dac1[14]} {dac_pattern_gen_inst/r_fdcs_repeat_dac1[15]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
#set_property port_width 16 [get_debug_ports u_ila_0/probe1]
#connect_debug_port u_ila_0/probe1 [get_nets [list {dac_pattern_gen_inst/r_fdcs_data_dac0[0]} {dac_pattern_gen_inst/r_fdcs_data_dac0[1]} {dac_pattern_gen_inst/r_fdcs_data_dac0[2]} {dac_pattern_gen_inst/r_fdcs_data_dac0[3]} {dac_pattern_gen_inst/r_fdcs_data_dac0[4]} {dac_pattern_gen_inst/r_fdcs_data_dac0[5]} {dac_pattern_gen_inst/r_fdcs_data_dac0[6]} {dac_pattern_gen_inst/r_fdcs_data_dac0[7]} {dac_pattern_gen_inst/r_fdcs_data_dac0[8]} {dac_pattern_gen_inst/r_fdcs_data_dac0[9]} {dac_pattern_gen_inst/r_fdcs_data_dac0[10]} {dac_pattern_gen_inst/r_fdcs_data_dac0[11]} {dac_pattern_gen_inst/r_fdcs_data_dac0[12]} {dac_pattern_gen_inst/r_fdcs_data_dac0[13]} {dac_pattern_gen_inst/r_fdcs_data_dac0[14]} {dac_pattern_gen_inst/r_fdcs_data_dac0[15]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe2]
#set_property port_width 16 [get_debug_ports u_ila_0/probe2]
#connect_debug_port u_ila_0/probe2 [get_nets [list {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[0]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[1]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[2]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[3]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[4]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[5]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[6]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[7]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[8]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[9]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[10]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[11]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[12]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[13]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[14]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac0[15]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe3]
#set_property port_width 16 [get_debug_ports u_ila_0/probe3]
#connect_debug_port u_ila_0/probe3 [get_nets [list {DAC0_DAT[0]} {DAC0_DAT[1]} {DAC0_DAT[2]} {DAC0_DAT[3]} {DAC0_DAT[4]} {DAC0_DAT[5]} {DAC0_DAT[6]} {DAC0_DAT[7]} {DAC0_DAT[8]} {DAC0_DAT[9]} {DAC0_DAT[10]} {DAC0_DAT[11]} {DAC0_DAT[12]} {DAC0_DAT[13]} {DAC0_DAT[14]} {DAC0_DAT[15]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe4]
#set_property port_width 1 [get_debug_ports u_ila_0/probe4]
#connect_debug_port u_ila_0/probe4 [get_nets [list dac_pattern_gen_inst/flag_fdcs_run_dac0]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe5]
#set_property port_width 1 [get_debug_ports u_ila_0/probe5]
#connect_debug_port u_ila_0/probe5 [get_nets [list dac_pattern_gen_inst/o_dac0_active_dco]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe1]
#set_property port_width 16 [get_debug_ports u_ila_1/probe1]
#connect_debug_port u_ila_1/probe1 [get_nets [list {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[0]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[1]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[2]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[3]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[4]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[5]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[6]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[7]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[8]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[9]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[10]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[11]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[12]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[13]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[14]} {dac_pattern_gen_inst/r_fdcs_cnt_repeat_dac1[15]}]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe2]
#set_property port_width 16 [get_debug_ports u_ila_1/probe2]
#connect_debug_port u_ila_1/probe2 [get_nets [list {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[0]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[1]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[2]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[3]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[4]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[5]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[6]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[7]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[8]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[9]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[10]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[11]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[12]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[13]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[14]} {dac_pattern_gen_inst/r_fdcs_cnt_duration_dac1[15]}]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe3]
#set_property port_width 16 [get_debug_ports u_ila_1/probe3]
#connect_debug_port u_ila_1/probe3 [get_nets [list {DAC1_DAT[0]} {DAC1_DAT[1]} {DAC1_DAT[2]} {DAC1_DAT[3]} {DAC1_DAT[4]} {DAC1_DAT[5]} {DAC1_DAT[6]} {DAC1_DAT[7]} {DAC1_DAT[8]} {DAC1_DAT[9]} {DAC1_DAT[10]} {DAC1_DAT[11]} {DAC1_DAT[12]} {DAC1_DAT[13]} {DAC1_DAT[14]} {DAC1_DAT[15]}]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe4]
#set_property port_width 1 [get_debug_ports u_ila_1/probe4]
#connect_debug_port u_ila_1/probe4 [get_nets [list dac_pattern_gen_inst/flag_fdcs_run_dac1]]
#create_debug_port u_ila_1 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe5]
#set_property port_width 1 [get_debug_ports u_ila_1/probe5]
#connect_debug_port u_ila_1/probe5 [get_nets [list dac_pattern_gen_inst/o_dac1_active_dco]]
#create_debug_port u_ila_2 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe1]
#set_property port_width 32 [get_debug_ports u_ila_2/probe1]
#connect_debug_port u_ila_2/probe1 [get_nets [list {IO_address[0]} {IO_address[1]} {IO_address[2]} {IO_address[3]} {IO_address[4]} {IO_address[5]} {IO_address[6]} {IO_address[7]} {IO_address[8]} {IO_address[9]} {IO_address[10]} {IO_address[11]} {IO_address[12]} {IO_address[13]} {IO_address[14]} {IO_address[15]} {IO_address[16]} {IO_address[17]} {IO_address[18]} {IO_address[19]} {IO_address[20]} {IO_address[21]} {IO_address[22]} {IO_address[23]} {IO_address[24]} {IO_address[25]} {IO_address[26]} {IO_address[27]} {IO_address[28]} {IO_address[29]} {IO_address[30]} {IO_address[31]}]]
#create_debug_port u_ila_2 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe2]
#set_property port_width 32 [get_debug_ports u_ila_2/probe2]
#connect_debug_port u_ila_2/probe2 [get_nets [list {IO_read_data[0]} {IO_read_data[1]} {IO_read_data[2]} {IO_read_data[3]} {IO_read_data[4]} {IO_read_data[5]} {IO_read_data[6]} {IO_read_data[7]} {IO_read_data[8]} {IO_read_data[9]} {IO_read_data[10]} {IO_read_data[11]} {IO_read_data[12]} {IO_read_data[13]} {IO_read_data[14]} {IO_read_data[15]} {IO_read_data[16]} {IO_read_data[17]} {IO_read_data[18]} {IO_read_data[19]} {IO_read_data[20]} {IO_read_data[21]} {IO_read_data[22]} {IO_read_data[23]} {IO_read_data[24]} {IO_read_data[25]} {IO_read_data[26]} {IO_read_data[27]} {IO_read_data[28]} {IO_read_data[29]} {IO_read_data[30]} {IO_read_data[31]}]]
#create_debug_port u_ila_2 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe3]
#set_property port_width 1 [get_debug_ports u_ila_2/probe3]
#connect_debug_port u_ila_2/probe3 [get_nets [list IO_addr_strobe]]
#create_debug_port u_ila_2 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe4]
#set_property port_width 1 [get_debug_ports u_ila_2/probe4]
#connect_debug_port u_ila_2/probe4 [get_nets [list IO_read_strobe]]
#create_debug_port u_ila_2 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe5]
#set_property port_width 1 [get_debug_ports u_ila_2/probe5]
#connect_debug_port u_ila_2/probe5 [get_nets [list IO_ready]]
#create_debug_port u_ila_2 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe6]
#set_property port_width 1 [get_debug_ports u_ila_2/probe6]
#connect_debug_port u_ila_2/probe6 [get_nets [list IO_write_strobe]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe1]
#set_property port_width 16 [get_debug_ports u_ila_3/probe1]
#connect_debug_port u_ila_3/probe1 [get_nets [list {dac_pattern_gen_inst/r_fdcs_repeat_dac0[0]} {dac_pattern_gen_inst/r_fdcs_repeat_dac0[1]} {dac_pattern_gen_inst/r_fdcs_repeat_dac0[2]} {dac_pattern_gen_inst/r_fdcs_repeat_dac0[3]} {dac_pattern_gen_inst/r_fdcs_repeat_dac0[4]} {dac_pattern_gen_inst/r_fdcs_repeat_dac0[5]} {dac_pattern_gen_inst/r_fdcs_repeat_dac0[6]} {dac_pattern_gen_inst/r_fdcs_repeat_dac0[7]} {dac_pattern_gen_inst/r_fdcs_repeat_dac0[8]} {dac_pattern_gen_inst/r_fdcs_repeat_dac0[9]} {dac_pattern_gen_inst/r_fdcs_repeat_dac0[10]} {dac_pattern_gen_inst/r_fdcs_repeat_dac0[11]} {dac_pattern_gen_inst/r_fdcs_repeat_dac0[12]} {dac_pattern_gen_inst/r_fdcs_repeat_dac0[13]} {dac_pattern_gen_inst/r_fdcs_repeat_dac0[14]} {dac_pattern_gen_inst/r_fdcs_repeat_dac0[15]}]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe2]
#set_property port_width 8 [get_debug_ports u_ila_3/probe2]
#connect_debug_port u_ila_3/probe2 [get_nets [list {master_spi_ad9783_inst/r_rd_D[0]} {master_spi_ad9783_inst/r_rd_D[1]} {master_spi_ad9783_inst/r_rd_D[2]} {master_spi_ad9783_inst/r_rd_D[3]} {master_spi_ad9783_inst/r_rd_D[4]} {master_spi_ad9783_inst/r_rd_D[5]} {master_spi_ad9783_inst/r_rd_D[6]} {master_spi_ad9783_inst/r_rd_D[7]}]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe3]
#set_property port_width 8 [get_debug_ports u_ila_3/probe3]
#connect_debug_port u_ila_3/probe3 [get_nets [list {master_spi_ad9783_inst/state[0]} {master_spi_ad9783_inst/state[1]} {master_spi_ad9783_inst/state[2]} {master_spi_ad9783_inst/state[3]} {master_spi_ad9783_inst/state[4]} {master_spi_ad9783_inst/state[5]} {master_spi_ad9783_inst/state[6]} {master_spi_ad9783_inst/state[7]}]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe4]
#set_property port_width 8 [get_debug_ports u_ila_3/probe4]
#connect_debug_port u_ila_3/probe4 [get_nets [list {master_spi_ad9516_inst/state[0]} {master_spi_ad9516_inst/state[1]} {master_spi_ad9516_inst/state[2]} {master_spi_ad9516_inst/state[3]} {master_spi_ad9516_inst/state[4]} {master_spi_ad9516_inst/state[5]} {master_spi_ad9516_inst/state[6]} {master_spi_ad9516_inst/state[7]}]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe5]
#set_property port_width 8 [get_debug_ports u_ila_3/probe5]
#connect_debug_port u_ila_3/probe5 [get_nets [list {master_spi_ad9516_inst/r_rd_D[0]} {master_spi_ad9516_inst/r_rd_D[1]} {master_spi_ad9516_inst/r_rd_D[2]} {master_spi_ad9516_inst/r_rd_D[3]} {master_spi_ad9516_inst/r_rd_D[4]} {master_spi_ad9516_inst/r_rd_D[5]} {master_spi_ad9516_inst/r_rd_D[6]} {master_spi_ad9516_inst/r_rd_D[7]}]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe6]
#set_property port_width 8 [get_debug_ports u_ila_3/probe6]
#connect_debug_port u_ila_3/probe6 [get_nets [list {master_spi_mcp23s17_inst/r_rd_DB[0]} {master_spi_mcp23s17_inst/r_rd_DB[1]} {master_spi_mcp23s17_inst/r_rd_DB[2]} {master_spi_mcp23s17_inst/r_rd_DB[3]} {master_spi_mcp23s17_inst/r_rd_DB[4]} {master_spi_mcp23s17_inst/r_rd_DB[5]} {master_spi_mcp23s17_inst/r_rd_DB[6]} {master_spi_mcp23s17_inst/r_rd_DB[7]}]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe7]
#set_property port_width 8 [get_debug_ports u_ila_3/probe7]
#connect_debug_port u_ila_3/probe7 [get_nets [list {master_spi_mcp23s17_inst/r_rd_DA[0]} {master_spi_mcp23s17_inst/r_rd_DA[1]} {master_spi_mcp23s17_inst/r_rd_DA[2]} {master_spi_mcp23s17_inst/r_rd_DA[3]} {master_spi_mcp23s17_inst/r_rd_DA[4]} {master_spi_mcp23s17_inst/r_rd_DA[5]} {master_spi_mcp23s17_inst/r_rd_DA[6]} {master_spi_mcp23s17_inst/r_rd_DA[7]}]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe8]
#set_property port_width 8 [get_debug_ports u_ila_3/probe8]
#connect_debug_port u_ila_3/probe8 [get_nets [list {master_spi_mcp23s17_inst/state[0]} {master_spi_mcp23s17_inst/state[1]} {master_spi_mcp23s17_inst/state[2]} {master_spi_mcp23s17_inst/state[3]} {master_spi_mcp23s17_inst/state[4]} {master_spi_mcp23s17_inst/state[5]} {master_spi_mcp23s17_inst/state[6]} {master_spi_mcp23s17_inst/state[7]}]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe9]
#set_property port_width 32 [get_debug_ports u_ila_3/probe9]
#connect_debug_port u_ila_3/probe9 [get_nets [list {w_TIMESTAMP_WO[0]} {w_TIMESTAMP_WO[1]} {w_TIMESTAMP_WO[2]} {w_TIMESTAMP_WO[3]} {w_TIMESTAMP_WO[4]} {w_TIMESTAMP_WO[5]} {w_TIMESTAMP_WO[6]} {w_TIMESTAMP_WO[7]} {w_TIMESTAMP_WO[8]} {w_TIMESTAMP_WO[9]} {w_TIMESTAMP_WO[10]} {w_TIMESTAMP_WO[11]} {w_TIMESTAMP_WO[12]} {w_TIMESTAMP_WO[13]} {w_TIMESTAMP_WO[14]} {w_TIMESTAMP_WO[15]} {w_TIMESTAMP_WO[16]} {w_TIMESTAMP_WO[17]} {w_TIMESTAMP_WO[18]} {w_TIMESTAMP_WO[19]} {w_TIMESTAMP_WO[20]} {w_TIMESTAMP_WO[21]} {w_TIMESTAMP_WO[22]} {w_TIMESTAMP_WO[23]} {w_TIMESTAMP_WO[24]} {w_TIMESTAMP_WO[25]} {w_TIMESTAMP_WO[26]} {w_TIMESTAMP_WO[27]} {w_TIMESTAMP_WO[28]} {w_TIMESTAMP_WO[29]} {w_TIMESTAMP_WO[30]} {w_TIMESTAMP_WO[31]}]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe10]
#set_property port_width 1 [get_debug_ports u_ila_3/probe10]
#connect_debug_port u_ila_3/probe10 [get_nets [list CLKD_CS_B]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe11]
#set_property port_width 1 [get_debug_ports u_ila_3/probe11]
#connect_debug_port u_ila_3/probe11 [get_nets [list CLKD_SCLK]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe12]
#set_property port_width 1 [get_debug_ports u_ila_3/probe12]
#connect_debug_port u_ila_3/probe12 [get_nets [list CLKD_SDIO]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe13]
#set_property port_width 1 [get_debug_ports u_ila_3/probe13]
#connect_debug_port u_ila_3/probe13 [get_nets [list CLKD_SDO]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe14]
#set_property port_width 1 [get_debug_ports u_ila_3/probe14]
#connect_debug_port u_ila_3/probe14 [get_nets [list DAC0_CS]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe15]
#set_property port_width 1 [get_debug_ports u_ila_3/probe15]
#connect_debug_port u_ila_3/probe15 [get_nets [list DAC1_CS]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe16]
#set_property port_width 1 [get_debug_ports u_ila_3/probe16]
#connect_debug_port u_ila_3/probe16 [get_nets [list DACx_RST_B]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe17]
#set_property port_width 1 [get_debug_ports u_ila_3/probe17]
#connect_debug_port u_ila_3/probe17 [get_nets [list DACx_SCLK]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe18]
#set_property port_width 1 [get_debug_ports u_ila_3/probe18]
#connect_debug_port u_ila_3/probe18 [get_nets [list DACx_SDIO]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe19]
#set_property port_width 1 [get_debug_ports u_ila_3/probe19]
#connect_debug_port u_ila_3/probe19 [get_nets [list DACx_SDO]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe20]
#set_property port_width 1 [get_debug_ports u_ila_3/probe20]
#connect_debug_port u_ila_3/probe20 [get_nets [list dac_pattern_gen_inst/o_dac0_active_clk]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe21]
#set_property port_width 1 [get_debug_ports u_ila_3/probe21]
#connect_debug_port u_ila_3/probe21 [get_nets [list dac_pattern_gen_inst/o_dac1_active_clk]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe22]
#set_property port_width 1 [get_debug_ports u_ila_3/probe22]
#connect_debug_port u_ila_3/probe22 [get_nets [list master_spi_ad9516_inst/r_done_SPI_frame]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe23]
#set_property port_width 1 [get_debug_ports u_ila_3/probe23]
#connect_debug_port u_ila_3/probe23 [get_nets [list master_spi_mcp23s17_inst/r_done_SPI_frame]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe24]
#set_property port_width 1 [get_debug_ports u_ila_3/probe24]
#connect_debug_port u_ila_3/probe24 [get_nets [list master_spi_ad9783_inst/r_done_SPI_frame]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe25]
#set_property port_width 1 [get_debug_ports u_ila_3/probe25]
#connect_debug_port u_ila_3/probe25 [get_nets [list dac_pattern_gen_inst/r_fdcs_run]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe26]
#set_property port_width 1 [get_debug_ports u_ila_3/probe26]
#connect_debug_port u_ila_3/probe26 [get_nets [list master_spi_mcp23s17_inst/r_trig_SPI_frame]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe27]
#set_property port_width 1 [get_debug_ports u_ila_3/probe27]
#connect_debug_port u_ila_3/probe27 [get_nets [list master_spi_ad9516_inst/r_trig_SPI_frame]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe28]
#set_property port_width 1 [get_debug_ports u_ila_3/probe28]
#connect_debug_port u_ila_3/probe28 [get_nets [list master_spi_ad9783_inst/r_trig_SPI_frame]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe29]
#set_property port_width 1 [get_debug_ports u_ila_3/probe29]
#connect_debug_port u_ila_3/probe29 [get_nets [list SPIO0_CS]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe30]
#set_property port_width 1 [get_debug_ports u_ila_3/probe30]
#connect_debug_port u_ila_3/probe30 [get_nets [list SPIO1_CS]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe31]
#set_property port_width 1 [get_debug_ports u_ila_3/probe31]
#connect_debug_port u_ila_3/probe31 [get_nets [list SPIOx_MISO]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe32]
#set_property port_width 1 [get_debug_ports u_ila_3/probe32]
#connect_debug_port u_ila_3/probe32 [get_nets [list SPIOx_MOSI]]
#create_debug_port u_ila_3 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_3/probe33]
#set_property port_width 1 [get_debug_ports u_ila_3/probe33]
#connect_debug_port u_ila_3/probe33 [get_nets [list SPIOx_SCLK]]
#set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
#set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
#set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
#connect_debug_port dbg_hub/clk [get_nets sys_clk]
