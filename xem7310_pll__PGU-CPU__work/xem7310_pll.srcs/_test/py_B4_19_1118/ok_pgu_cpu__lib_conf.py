## ok module for python 3.5
## works both with x64 and win32
## 
##   Windows DLL Usage
##   http://www.opalkelly.com:8090/display/FPSDK/Programming+Languages
##   http://www.microsoft.com/en-us/download/details.aspx?id=40784
##
## https://www.opalkelly.com/examples/home/
## https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel.html
## http://www.opalkelly.com:8090/display/FPSDK/Getting+Started
## wxPython 2.8 http://www.wxpython.org
#
## ok_pgu_cpu__lib_conf.py : library configuration ... 
##                           end-point addresses  for PGU-CPU-F5500


## OK end-point addresses : OK_EP_ADRS_CONFIG
OK_EP_ADRS_CONFIG = {
	'board_name'         : 'PGU-CPU-F5500',
	#'ver'                : '0xB2191112', # SPIO test
	#'ver'                : '0xB3191114', # CLKD test
	'ver'                : '0xB4191118', # DAC IO test
	#'bit_filename'       : 'xem7310__pgu_cpu__top.bit', 
	'bit_filename'       : '../img_B4_19_1118/xem7310__pgu_cpu__top.bit', 
	#
	# wire-in
	'SW_BUILD_ID'        : 0x00, #PGU
	'TEST_CON'           : 0x01, #PGU
	'TEST_CC_DIN'        : 0x02,
	'TEST_IO_CON'        : 0x03, #PGU
	'DACX_DAT_WI'        : 0x04, #PGU
	'DACX_WI'            : 0x05, #PGU
	'CLKD_WI'            : 0x06, #PGU
	'SPIO_WI'            : 0x07, #PGU
	'SPO_DIN_B0_L'       : 0x08,
	'SPO_DIN_B0_H'       : 0x09,
	'SPO_DIN_B1_L'       : 0x0A,
	'SPO_DIN_B1_H'       : 0x0B,
	'SPO_DIN_B2_L'       : 0x0C,
	'SPO_DIN_B2_H'       : 0x0D,
	'SPO_DIN_B3_L'       : 0x0E,
	'SPO_DIN_B3_H'       : 0x0F,
	'DAC_A2A3_CON'       : 0x10,
	'DAC_BIAS_CON'       : 0x11,
	'wi12'               : 0x12,
	'wi13'               : 0x13,
	'DAC_A2A3_DIN21'     : 0x14,
	'DAC_A2A3_DIN43'     : 0x15,
	'DAC_BIAS_DIN21'     : 0x16,
	'DAC_BIAS_DIN43'     : 0x17,
	'ADC_HS_WI'          : 0x18,
	'wi19'               : 0x19,
	'wi1A'               : 0x1A,
	'wi1B'               : 0x1B,
	'wi1C'               : 0x1C,
	'ADC_HS_UPD_SMP'     : 0x1D,
	'ADC_HS_SMP_PRD'     : 0x1E,
	'ADC_HS_DLY_TAP_OPT' : 0x1F,
	# wire-out
	'FPGA_IMAGE_ID'      : 0x20, #PGU
	'TEST_OUT'           : 0x21,
	'TEST_CC_MON'        : 0x22,
	'TEST_IO_MON'        : 0x23, #PGU
	'DAC_TEST_OUT'       : 0x24,
	'DACX_WO'            : 0x25, #PGU
	'CLKD_WO'            : 0x26, #PGU
	'SPIO_WO'            : 0x27, #PGU
	'SPO_MON_B0_L'       : 0x28,
	'SPO_MON_B0_H'       : 0x29,
	'SPO_MON_B1_L'       : 0x2A,
	'SPO_MON_B1_H'       : 0x2B,
	'SPO_MON_B2_L'       : 0x2C,
	'SPO_MON_B2_H'       : 0x2D,
	'SPO_MON_B3_L'       : 0x2E,
	'SPO_MON_B3_H'       : 0x2F,
	'DAC_A2A3_FLAG'      : 0x30,
	'DAC_BIAS_FLAG'      : 0x31,
	'DAC_TEST_RB1'       : 0x32,
	'DAC_TEST_RB2'       : 0x33,
	'DAC_A2A3_RB21'      : 0x34,
	'DAC_A2A3_RB43'      : 0x35,
	'DAC_BIAS_RB21'      : 0x36,
	'DAC_BIAS_RB43'      : 0x37,
	'ADC_HS_WO'          : 0x38,
	'ADC_BASE_FREQ'	     : 0x39,
	'XADC_TEMP'          : 0x3A, #PGU
	'XADC_VOLT'          : 0x3B, #PGU
	'ADC_HS_DOUT0'       : 0x3C,
	'ADC_HS_DOUT1'       : 0x3D,
	'ADC_HS_DOUT2'       : 0x3E,
	'ADC_HS_DOUT3'       : 0x3F,
	#
	# trig-in
	'TEST_TI'            : 0x40, #PGU
	'TEST_TI_HS'         : 0x41, 
	'TEST_IO_TI'         : 0x43, #PGU
	'DACX_TI'            : 0x45, #PGU
	'CLKD_TI'            : 0x46, #PGU
	'SPIO_TI'            : 0x47, #PGU
	'DAC_BIAS_TI'        : 0x50,
	'DAC_A2A3_TI'        : 0x51,
	'ADC_HS_TI'          : 0x58,
	#
	# trig-out
	'TEST_TO'            : 0x60,
	'DAC_BIAS_TO'        : 0x70,
	'DAC_A2A3_TO'        : 0x71,
	'ADC_HS_TO'          : 0x78,
	#
	# pipe-out
	'ADC_HS_DOUT0_PO'    : 0xBC,
	'ADC_HS_DOUT1_PO'    : 0xBD,
	'ADC_HS_DOUT2_PO'    : 0xBE,
	'ADC_HS_DOUT3_PO'    : 0xBF,
	#
	'end' : 'end'
}

