## ok module for python 3.5
## works both with x64 and win32
## 
##   Windows DLL Usage
##   http://www.opalkelly.com:8090/display/FPSDK/Programming+Languages
##   http://www.microsoft.com/en-us/download/details.aspx?id=40784
##
## https://www.opalkelly.com/examples/home/
## https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel.html
## http://www.opalkelly.com:8090/display/FPSDK/Getting+Started
## wxPython 2.8 http://www.wxpython.org
#
## ok_pgu_cpu__04_spio.py : test code for SPIO MCP23S17
#  

####
## controls
#  
#  
FPGA_CONFIGURE = 0
#
#BIT_FILENAME = ''; 
#BIT_FILENAME = '../img_B3_19_1114/xem7310__pgu_cpu__top.bit'
#
####

####
## library call
import ok_pgu_cpu__lib as pgu
#
# display board conf data in library  
ret = pgu.display_conf_header()
print(ret)
#
# set bit filename including path 
BIT_FILENAME = pgu.conf.OK_EP_ADRS_CONFIG['bit_filename']
#
####

####
## init : dev
dev = pgu.ok_init()
print(dev)
####

####
ret = pgu.ok_caller_id()
print(ret)
####

####
## open
FPGA_SERIAL = '' # for any
#FPGA_SERIAL = '1739000J7V' # for module: CMU-CPU-F5500-FPGA1
#FPGA_SERIAL = '1908000OVP' # for module: CMU-CPU-F5500-FPGA2
#FPGA_SERIAL = '1739000J8A' # for module: CMU-CPU-F5500-FPGA3
#FPGA_SERIAL = '1739000J63' # for module: CMU-CPU-F5500-FPGA4
#FPGA_SERIAL = '1739000J8I' # for module: CMU-CPU-F5500-FPGA5
#
ret = pgu.ok_open(FPGA_SERIAL)
print(ret)
####


####
## FPGA_CONFIGURE
if FPGA_CONFIGURE==1: 
	ret = pgu.ok_conf(BIT_FILENAME)
	print(ret)
	print('Downloading bit file makes your debugger be reset!!')
####  

####
## read fpga_image_id
fpga_image_id__str = pgu.read_fpga_image_id()
print(fpga_image_id__str)
####

####
## read FPGA internal temp and volt
ret = pgu.monitor_fpga()
print(ret)
####

####
## test counter on 
ret = pgu.test_counter('ON')
print(ret)
####

##--------------------------------------------------##

####
## SPIO init
ret = pgu.sp_x_init()
print(ret)
####

from ok_pgu_cpu__lib import form_hex_32b

####
## SPIO send spi frame 
ret = pgu.spio_send_spi_frame(0x1100FFF7)
print(form_hex_32b(ret))
ret = pgu.sp_0_reg_write_b16(0x00,0xFFFF)
print(form_hex_32b(ret))
ret = pgu.sp_0_reg_read_b16(0x00)
print(form_hex_32b(ret))
ret = pgu.sp_1_reg_write_b16(0x00,0xFFFF)
print(form_hex_32b(ret))
ret = pgu.sp_1_reg_read_b16(0x00)
print(form_hex_32b(ret))
####

####
## LED12 (RED) and power control test  
#
##// LED test with USER_LED_ST0 net (SP1_GPB3)
##//   SP1 IODIR write frame : w_SPIO_WI = 32'h10_00_FF_F7
##//   SP1 IODIR read  frame : w_SPIO_WI = 32'h11_00_XX_XX
##//   SP1 GPIO  write frame : w_SPIO_WI = 32'h10_12_00_08
##//   SP1 GPIO  write frame : w_SPIO_WI = 32'h10_12_00_00
##//   SP1 GPIO  read  frame : w_SPIO_WI = 32'h11_12_XX_XX
##//   SP1 OLAT  write frame : w_SPIO_WI = 32'h10_14_00_00
##//   SP1 OLAT  read  frame : w_SPIO_WI = 32'h11_14_XX_XX
#
pgu.spio_ext__pwr_led(led=1,pwr_dac=1,pwr_adc=0,pwr_amp=0)
#
# set SP1_GPB3 IODIR output 
#ret = pgu.sp_1_reg_write_b16(0x00,0xFFF7)
#print(form_hex_32b(ret))
# set SP1_GPB3 GPIO output to 1
#ret = pgu.sp_1_reg_write_b16(0x12,0x0008)
#print(form_hex_32b(ret))
#
####
pgu.sleep(1)
####
#
pgu.spio_ext__pwr_led(led=0,pwr_dac=0,pwr_adc=0,pwr_amp=0)
#
# set SP1_GPB3 GPIO output to 0
#ret = pgu.sp_1_reg_write_b16(0x12,0x0000)
#print(form_hex_32b(ret))
# set SP1_GPB3 IODIR input 
#ret = pgu.sp_1_reg_write_b16(0x00,0xFFFF)
#print(form_hex_32b(ret))
#
####




##--------------------------------------------------##

####
pgu.sleep(3)
####

####
## test counter off
ret = pgu.test_counter('OFF')
print(ret)
####

####
## test counter reset
ret = pgu.test_counter('RESET')
print(ret)
####


####
## close
ret = pgu.ok_close()
print(ret)
####