## ok module for python 3.5
## works both with x64 and win32
## 
##   Windows DLL Usage
##   http://www.opalkelly.com:8090/display/FPSDK/Programming+Languages
##   http://www.microsoft.com/en-us/download/details.aspx?id=40784
##
## https://www.opalkelly.com/examples/home/
## https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel.html
## http://www.opalkelly.com:8090/display/FPSDK/Getting+Started
## wxPython 2.8 http://www.wxpython.org
#
## ok_pgu_cpu__02_conf.py : test code for FPGA configuration

####
## controls
#  
#  
FPGA_CONFIGURE = 1
#
#BIT_FILENAME = ''; 
#BIT_FILENAME = '../img_B3_19_1114/xem7310__pgu_cpu__top.bit'
#
####

####
## library call
import ok_pgu_cpu__lib as pgu
#
# display board conf data in library  
ret = pgu.display_conf_header()
print(ret)
#
# set bit filename including path 
BIT_FILENAME = pgu.conf.OK_EP_ADRS_CONFIG['bit_filename']
#
####

####
## init : dev
dev = pgu.ok_init()
print(dev)
####

####
ret = pgu.ok_caller_id()
print(ret)
####

####
## open
FPGA_SERIAL = '' # for any
#FPGA_SERIAL = '1739000J7V' # for module: CMU-CPU-F5500-FPGA1
#FPGA_SERIAL = '1908000OVP' # for module: CMU-CPU-F5500-FPGA2
#FPGA_SERIAL = '1739000J8A' # for module: CMU-CPU-F5500-FPGA3
#FPGA_SERIAL = '1739000J63' # for module: CMU-CPU-F5500-FPGA4
#FPGA_SERIAL = '1739000J8I' # for module: CMU-CPU-F5500-FPGA5
#
ret = pgu.ok_open(FPGA_SERIAL)
print(ret)
####


####
## FPGA_CONFIGURE
if FPGA_CONFIGURE==1: 
	ret = pgu.ok_conf(BIT_FILENAME)
	print(ret)
####  

####
## read fpga_image_id
fpga_image_id__str = pgu.read_fpga_image_id()
print(fpga_image_id__str)
####


####
# work above!
####

####
pgu.sleep(3)
####

####
## close
ret = pgu.ok_close()
print(ret)
####