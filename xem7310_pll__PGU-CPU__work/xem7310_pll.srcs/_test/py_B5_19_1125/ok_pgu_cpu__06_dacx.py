## ok module for python 3.5
## works both with x64 and win32
## 
##   Windows DLL Usage
##   http://www.opalkelly.com:8090/display/FPSDK/Programming+Languages
##   http://www.microsoft.com/en-us/download/details.aspx?id=40784
##
## https://www.opalkelly.com/examples/home/
## https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel.html
## http://www.opalkelly.com:8090/display/FPSDK/Getting+Started
## wxPython 2.8 http://www.wxpython.org
#
## ok_pgu_cpu__06_dacx.py : test code for DACX AD9783
#  

####
## controls
#  
#  
FPGA_CONFIGURE = 1
#
#BIT_FILENAME = ''; 
#
####

####
## library call
import ok_pgu_cpu__lib as pgu
#
# display board conf data in library  
ret = pgu.display_conf_header()
print(ret)
#
# set bit filename including path 
BIT_FILENAME = pgu.conf.OK_EP_ADRS_CONFIG['bit_filename']
#
####

####
## init : dev
dev = pgu.ok_init()
print(dev)
####

####
ret = pgu.ok_caller_id()
print(ret)
####

####
## open
FPGA_SERIAL = '' # for any
#FPGA_SERIAL = '1739000J7V' # for module: CMU-CPU-F5500-FPGA1
#FPGA_SERIAL = '1908000OVP' # for module: CMU-CPU-F5500-FPGA2
#FPGA_SERIAL = '1739000J8A' # for module: CMU-CPU-F5500-FPGA3
#FPGA_SERIAL = '1739000J63' # for module: CMU-CPU-F5500-FPGA4
#FPGA_SERIAL = '1739000J8I' # for module: CMU-CPU-F5500-FPGA5
#
ret = pgu.ok_open(FPGA_SERIAL)
print(ret)
####


####
## FPGA_CONFIGURE
if FPGA_CONFIGURE==1: 
	ret = pgu.ok_conf(BIT_FILENAME)
	print(ret)
	print('Downloading bit file makes your debugger be reset!!')
####  

####
## read fpga_image_id
fpga_image_id__str = pgu.read_fpga_image_id()
print(fpga_image_id__str)
####

####
## read FPGA internal temp and volt
ret = pgu.monitor_fpga()
print(ret)
####

####
## test counter on 
ret = pgu.test_counter('ON')
print(ret)
####

####
## power control on : LED, PWR_DAC
#
print('> power on: led=1,pwr_dac=1,pwr_adc=0,pwr_amp=0')
pgu.spio_ext__pwr_led(led=1,pwr_dac=1,pwr_adc=0,pwr_amp=1)
####

####
## CLKD on : 400MHz clock output
#
print('> CLKD test')
pgu.clkd_init()
pgu.clkd_reg_read_b8(0x000) # readback 0x18
pgu.clkd_reg_read_b8(0x003) # read IC 0x41 

## CLKD reg check 
reg_0x000 = pgu.clkd_reg_read_b8(0x000)
reg_0x003 = pgu.clkd_reg_read_b8(0x003)
reg_0x010 = pgu.clkd_reg_read_b8(0x010)
reg_0x016 = pgu.clkd_reg_read_b8(0x016)
reg_0x017 = pgu.clkd_reg_read_b8(0x017)
reg_0x01A = pgu.clkd_reg_read_b8(0x01A)
reg_0x01B = pgu.clkd_reg_read_b8(0x01B)
reg_0x0F2 = pgu.clkd_reg_read_b8(0x0F2)
reg_0x0F3 = pgu.clkd_reg_read_b8(0x0F3)
reg_0x140 = pgu.clkd_reg_read_b8(0x140)
reg_0x142 = pgu.clkd_reg_read_b8(0x142)
reg_0x193 = pgu.clkd_reg_read_b8(0x193)
reg_0x194 = pgu.clkd_reg_read_b8(0x194)
reg_0x195 = pgu.clkd_reg_read_b8(0x195)
reg_0x199 = pgu.clkd_reg_read_b8(0x199)
reg_0x19B = pgu.clkd_reg_read_b8(0x19B)
reg_0x19C = pgu.clkd_reg_read_b8(0x19C)
reg_0x19E = pgu.clkd_reg_read_b8(0x19E)
reg_0x19F = pgu.clkd_reg_read_b8(0x19F)
reg_0x1A0 = pgu.clkd_reg_read_b8(0x1A0)
reg_0x1A1 = pgu.clkd_reg_read_b8(0x1A1)
reg_0x1A2 = pgu.clkd_reg_read_b8(0x1A2)
reg_0x1E1 = pgu.clkd_reg_read_b8(0x1E1)
reg_0x232 = pgu.clkd_reg_read_b8(0x232)
#
print('{} = 0x{:02X}'.format('reg_0x000',reg_0x000))
print('{} = 0x{:02X}'.format('reg_0x003',reg_0x003))
print('{} = 0x{:02X}'.format('reg_0x010',reg_0x010))
print('{} = 0x{:02X}'.format('reg_0x016',reg_0x016))
print('{} = 0x{:02X}'.format('reg_0x017',reg_0x017))
print('{} = 0x{:02X}'.format('reg_0x01A',reg_0x01A))
print('{} = 0x{:02X}'.format('reg_0x01B',reg_0x01B))
print('{} = 0x{:02X}'.format('reg_0x0F2',reg_0x0F2))
print('{} = 0x{:02X}'.format('reg_0x0F3',reg_0x0F3))
print('{} = 0x{:02X}'.format('reg_0x140',reg_0x140))
print('{} = 0x{:02X}'.format('reg_0x142',reg_0x142))
print('{} = 0x{:02X}'.format('reg_0x193',reg_0x193))
print('{} = 0x{:02X}'.format('reg_0x194',reg_0x194))
print('{} = 0x{:02X}'.format('reg_0x195',reg_0x195))
print('{} = 0x{:02X}'.format('reg_0x199',reg_0x199))
print('{} = 0x{:02X}'.format('reg_0x19B',reg_0x19B))
print('{} = 0x{:02X}'.format('reg_0x19C',reg_0x19C))
print('{} = 0x{:02X}'.format('reg_0x19E',reg_0x19E))
print('{} = 0x{:02X}'.format('reg_0x19F',reg_0x19F))
print('{} = 0x{:02X}'.format('reg_0x1A0',reg_0x1A0))
print('{} = 0x{:02X}'.format('reg_0x1A1',reg_0x1A1))
print('{} = 0x{:02X}'.format('reg_0x1A2',reg_0x1A2))
print('{} = 0x{:02X}'.format('reg_0x1E1',reg_0x1E1))
print('{} = 0x{:02X}'.format('reg_0x232',reg_0x232))

##  Settings for Clock Distribution : 
#reg_0x010 = pgu.clkd_reg_read_b8(0x010)
#reg_0x1E1 = pgu.clkd_reg_read_b8(0x1E1)
#pgu.clkd_reg_write_b8(0x010,(reg_0x010&0xFC)+0x01) # PLL power-down
#pgu.clkd_reg_write_b8(0x1E1,(reg_0x1E1&0xFC)+0x01) # Bypass VCO divider
#reg_0x010 = pgu.clkd_reg_read_b8(0x010)
#reg_0x1E1 = pgu.clkd_reg_read_b8(0x1E1)
#print('{} = 0x{:02X}'.format('reg_0x010',reg_0x010))
#print('{} = 0x{:02X}'.format('reg_0x1E1',reg_0x1E1))
#
pgu.clkd_reg_write_b8(0x010,0x7D) # PLL power-down
#
pgu.clkd_reg_write_b8(0x1E1,0x01) # Bypass VCO divider
#
#pgu.clkd_reg_write_b8(0x193,0xBB) # DVD1 div 2+11+11=24 --> DACx: 400MHz/24 = 16.7MHz # OK
#pgu.clkd_reg_write_b8(0x193,0x00) # DVD1 div 2+0+0=2 --> DACx: 400MHz/2 = 200MHz
#pgu.clkd_reg_write_b8(0x193,0x33) # DVD1 div 2+3+3=8 --> DACx: 400MHz/8 = 50MHz
#pgu.clkd_reg_write_b8(0x193,0x44) # DVD1 div 2+4+4=10 --> DACx: 400MHz/10 = 40MHz # NG with DAC0
#pgu.clkd_reg_write_b8(0x193,0x55) # DVD1 div 2+5+5=12 --> DACx: 400MHz/12 = 33.3MHz # NG
#pgu.clkd_reg_write_b8(0x193,0x77) # DVD1 div 2+7+7=16 --> DACx: 400MHz/16 = 25MHz # OK
pgu.clkd_reg_write_b8(0x193,0xFF) # DVD1 div 2+15+15=32 --> DACx: 400MHz/32 = 12.5MHz # OK
#
#pgu.clkd_reg_write_b8(0x194,0x80) # DVD1 bypass --> DACx: 400MHz/1 = 400MHz
#
pgu.clkd_reg_write_b8(0x0F3,0x08) # enable path for DAC1
#
#pgu.clkd_reg_write_b8(0x199,0x22) # DVD3.1 div 2+2+2=6 
#pgu.clkd_reg_write_b8(0x19B,0x11) # DVD3.2 div 2+1+1=4  --> REFo: 400MHz/24 = 16.7MHz
#
#pgu.clkd_reg_write_b8(0x199,0x00) # DVD3.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x19B,0x00) # DVD3.2 div 2+0+0=2  --> REFo: 400MHz/4 = 100MHz
#
#pgu.clkd_reg_write_b8(0x199,0x00) # DVD3.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x19B,0x11) # DVD3.2 div 2+1+1=4  --> REFo: 400MHz/8 = 50MHz
#
#pgu.clkd_reg_write_b8(0x199,0x00) # DVD3.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x19B,0x33) # DVD3.2 div 2+3+3=8  --> REFo: 400MHz/16 = 25MHz 
#
pgu.clkd_reg_write_b8(0x19C,0x30) # DVD3.1, DVD3.2 all bypass --> REFo: 400MHz/1 = 400MHz
#
#pgu.clkd_reg_write_b8(0x19E,0x22) # DVD4.1 div 2+2+2=6 
#pgu.clkd_reg_write_b8(0x1A0,0x11) # DVD4.2 div 2+1+1=4  --> FPGA: 400MHz/24 = 16.7MHz # OK
#
#pgu.clkd_reg_write_b8(0x19E,0x00) # DVD4.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x1A0,0x00) # DVD4.2 div 2+0+0=2  --> FPGA: 400MHz/4 = 100MHz
#
#pgu.clkd_reg_write_b8(0x19E,0x00) # DVD4.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x1A0,0x11) # DVD4.2 div 2+1+1=4  --> FPGA: 400MHz/8 = 50MHz
#
#pgu.clkd_reg_write_b8(0x19E,0x00) # DVD4.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x1A0,0x12) # DVD4.2 div 2+1+2=5  --> FPGA: 400MHz/10 = 40MHz #NG
#
#pgu.clkd_reg_write_b8(0x19E,0x00) # DVD4.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x1A0,0x22) # DVD4.2 div 2+2+2=6  --> FPGA: 400MHz/12 = 33.3MHz #NG
#
pgu.clkd_reg_write_b8(0x19E,0x00) # DVD4.1 div 2+0+0=2 
pgu.clkd_reg_write_b8(0x1A0,0x33) # DVD4.2 div 2+3+3=8  --> FPGA: 400MHz/16 = 25MHz # OK 
#
#pgu.clkd_reg_write_b8(0x1A1,0x30) # DVD4.1, DVD4.2 all bypass --> FPGA: 400MHz/1 = 400MHz
#
#pgu.clkd_reg_write_b8(0x142,0x44) # OUT8 drive current up 0x40(1.75mA) --> 0x42(3.5mA) --> 0x44(5.25mA) --> 0x46(7mA)
pgu.clkd_reg_write_b8(0x142,0x42) # OUT8 drive current up 0x40(1.75mA) --> 0x42(3.5mA) --> 0x44(5.25mA) --> 0x46(7mA)
#
pgu.clkd_reg_write_b8(0x232,0x01) # update registers
#
#
reg_0x010 = pgu.clkd_reg_read_b8(0x010)
reg_0x1E1 = pgu.clkd_reg_read_b8(0x1E1)
reg_0x194 = pgu.clkd_reg_read_b8(0x194)
reg_0x0F3 = pgu.clkd_reg_read_b8(0x0F3)
reg_0x19C = pgu.clkd_reg_read_b8(0x19C)
reg_0x1A1 = pgu.clkd_reg_read_b8(0x1A1)
reg_0x142 = pgu.clkd_reg_read_b8(0x142)
#
print('{} = 0x{:02X}'.format('reg_0x010',reg_0x010))
print('{} = 0x{:02X}'.format('reg_0x1E1',reg_0x1E1))
print('{} = 0x{:02X}'.format('reg_0x194',reg_0x194))
print('{} = 0x{:02X}'.format('reg_0x0F3',reg_0x0F3))
print('{} = 0x{:02X}'.format('reg_0x19C',reg_0x19C))
print('{} = 0x{:02X}'.format('reg_0x1A1',reg_0x1A1))
print('{} = 0x{:02X}'.format('reg_0x142',reg_0x142))


####
pgu.sleep(1)
####


##--------------------------------------------------##


## test DACX SPI frame 
print('> DACX test')
pgu.dacx_init()
#
# test write 
#pgu.dac0_reg_write_b8(0x0B,0xE8)
#pgu.dac1_reg_write_b8(0x0F,0xE9)
#pgu.dac0_reg_write_b8(0x0B,0xF9)
#pgu.dac0_reg_write_b8(0x0C,0x03)
#
reg_0x00 = pgu.dac0_reg_read_b8(0x00)
reg_0x01 = pgu.dac0_reg_read_b8(0x01)
reg_0x02 = pgu.dac0_reg_read_b8(0x02)
reg_0x03 = pgu.dac0_reg_read_b8(0x03)
reg_0x04 = pgu.dac0_reg_read_b8(0x04)
reg_0x05 = pgu.dac0_reg_read_b8(0x05)
reg_0x06 = pgu.dac0_reg_read_b8(0x06)
reg_0x07 = pgu.dac0_reg_read_b8(0x07)
reg_0x08 = pgu.dac0_reg_read_b8(0x08)
reg_0x09 = pgu.dac0_reg_read_b8(0x09)
reg_0x0A = pgu.dac0_reg_read_b8(0x0A)
reg_0x0B = pgu.dac0_reg_read_b8(0x0B)
reg_0x0C = pgu.dac0_reg_read_b8(0x0C)
reg_0x0D = pgu.dac0_reg_read_b8(0x0D)
reg_0x0E = pgu.dac0_reg_read_b8(0x0E)
reg_0x0F = pgu.dac0_reg_read_b8(0x0F)
reg_0x10 = pgu.dac0_reg_read_b8(0x10)
reg_0x11 = pgu.dac0_reg_read_b8(0x11)
reg_0x12 = pgu.dac0_reg_read_b8(0x12)
reg_0x1F = pgu.dac0_reg_read_b8(0x1F)
#
print('>> DAC0 reg:')
print('{} = 0x{:02X}'.format('reg_0x00',reg_0x00))
print('{} = 0x{:02X}'.format('reg_0x01',reg_0x01))
print('{} = 0x{:02X}'.format('reg_0x02',reg_0x02))
print('{} = 0x{:02X}'.format('reg_0x03',reg_0x03))
print('{} = 0x{:02X}'.format('reg_0x04',reg_0x04))
print('{} = 0x{:02X}'.format('reg_0x05',reg_0x05))
print('{} = 0x{:02X}'.format('reg_0x06',reg_0x06))
print('{} = 0x{:02X}'.format('reg_0x07',reg_0x07))
print('{} = 0x{:02X}'.format('reg_0x08',reg_0x08))
print('{} = 0x{:02X}'.format('reg_0x09',reg_0x09))
print('{} = 0x{:02X}'.format('reg_0x0A',reg_0x0A))
print('{} = 0x{:02X}'.format('reg_0x0B',reg_0x0B))
print('{} = 0x{:02X}'.format('reg_0x0C',reg_0x0C))
print('{} = 0x{:02X}'.format('reg_0x0D',reg_0x0D))
print('{} = 0x{:02X}'.format('reg_0x0E',reg_0x0E))
print('{} = 0x{:02X}'.format('reg_0x0F',reg_0x0F))
print('{} = 0x{:02X}'.format('reg_0x10',reg_0x10))
print('{} = 0x{:02X}'.format('reg_0x11',reg_0x11))
print('{} = 0x{:02X}'.format('reg_0x12',reg_0x12))
print('{} = 0x{:02X}'.format('reg_0x1F',reg_0x1F))
#
reg_0x00 = pgu.dac1_reg_read_b8(0x00)
reg_0x01 = pgu.dac1_reg_read_b8(0x01)
reg_0x02 = pgu.dac1_reg_read_b8(0x02)
reg_0x03 = pgu.dac1_reg_read_b8(0x03)
reg_0x04 = pgu.dac1_reg_read_b8(0x04)
reg_0x05 = pgu.dac1_reg_read_b8(0x05)
reg_0x06 = pgu.dac1_reg_read_b8(0x06)
reg_0x07 = pgu.dac1_reg_read_b8(0x07)
reg_0x08 = pgu.dac1_reg_read_b8(0x08)
reg_0x09 = pgu.dac1_reg_read_b8(0x09)
reg_0x0A = pgu.dac1_reg_read_b8(0x0A)
reg_0x0B = pgu.dac1_reg_read_b8(0x0B)
reg_0x0C = pgu.dac1_reg_read_b8(0x0C)
reg_0x0D = pgu.dac1_reg_read_b8(0x0D)
reg_0x0E = pgu.dac1_reg_read_b8(0x0E)
reg_0x0F = pgu.dac1_reg_read_b8(0x0F)
reg_0x10 = pgu.dac1_reg_read_b8(0x10)
reg_0x11 = pgu.dac1_reg_read_b8(0x11)
reg_0x12 = pgu.dac1_reg_read_b8(0x12)
reg_0x1F = pgu.dac1_reg_read_b8(0x1F)
#
print('>> DAC1 reg:')
print('{} = 0x{:02X}'.format('reg_0x00',reg_0x00))
print('{} = 0x{:02X}'.format('reg_0x01',reg_0x01))
print('{} = 0x{:02X}'.format('reg_0x02',reg_0x02))
print('{} = 0x{:02X}'.format('reg_0x03',reg_0x03))
print('{} = 0x{:02X}'.format('reg_0x04',reg_0x04))
print('{} = 0x{:02X}'.format('reg_0x05',reg_0x05))
print('{} = 0x{:02X}'.format('reg_0x06',reg_0x06))
print('{} = 0x{:02X}'.format('reg_0x07',reg_0x07))
print('{} = 0x{:02X}'.format('reg_0x08',reg_0x08))
print('{} = 0x{:02X}'.format('reg_0x09',reg_0x09))
print('{} = 0x{:02X}'.format('reg_0x0A',reg_0x0A))
print('{} = 0x{:02X}'.format('reg_0x0B',reg_0x0B))
print('{} = 0x{:02X}'.format('reg_0x0C',reg_0x0C))
print('{} = 0x{:02X}'.format('reg_0x0D',reg_0x0D))
print('{} = 0x{:02X}'.format('reg_0x0E',reg_0x0E))
print('{} = 0x{:02X}'.format('reg_0x0F',reg_0x0F))
print('{} = 0x{:02X}'.format('reg_0x10',reg_0x10))
print('{} = 0x{:02X}'.format('reg_0x11',reg_0x11))
print('{} = 0x{:02X}'.format('reg_0x12',reg_0x12))
print('{} = 0x{:02X}'.format('reg_0x1F',reg_0x1F))
#


# check lock detection
ret = pgu.read_TEST_IO_MON()
print(ret)
# 0x6000_0000 readback 
#assign w_TEST_IO_MON[28:27] =  2'b0;
#assign w_TEST_IO_MON[26] = dac1_dco_clk_locked;
#assign w_TEST_IO_MON[25] = dac0_dco_clk_locked;
#assign w_TEST_IO_MON[24] = clk_dac_locked;
#assign w_TEST_IO_MON[23:20] =  4'b0;
#assign w_TEST_IO_MON[19] = clk4_locked;
#assign w_TEST_IO_MON[18] = clk3_locked;
#assign w_TEST_IO_MON[17] = clk2_locked;
#assign w_TEST_IO_MON[16] = clk1_locked;
#assign w_TEST_IO_MON[15: 0] = 16'b0;
#
# readback 0xE10F0000 : FPGA - 16.7MHz
# readback 0xA70F0000 : FPGA - 16.7MHz, DACx - 16.7MHz
# note... 16.7MHz lock detected...
# note ... 50MHz fail...
# note ... 40MHz dac1 works, dac0 fails

# reset FPGA pll
pgu.dacx_fpga_pll_rst(clkd_out_rst=1, dac0_dco_rst=1, dac1_dco_rst=1)

# re-check lock detection
ret = pgu.read_TEST_IO_MON()
print(ret)

# run FPGA pll
pgu.dacx_fpga_pll_rst(clkd_out_rst=0, dac0_dco_rst=0, dac1_dco_rst=0)

# re-check lock detection
ret = pgu.read_TEST_IO_MON()
print(ret)


## test data 
#pgu.dacx_test_data_write_32b(dac0_val_b16=0x0000, dac1_val_b16=0x0000)
#input('>>> Press Enter to Next')
##pgu.dacx_test_data_write_32b(dac0_val_b16=0x7fff, dac1_val_b16=0x0fff) # 8V
##pgu.dacx_test_data_write_32b(dac0_val_b16=0x6fff, dac1_val_b16=0x0fff) # 7V
##pgu.dacx_test_data_write_32b(dac0_val_b16=0x5fff, dac1_val_b16=0x0fff) # 6V 
##pgu.dacx_test_data_write_32b(dac0_val_b16=0x4fff, dac1_val_b16=0x0fff) # 5V
##pgu.dacx_test_data_write_32b(dac0_val_b16=0x3fff, dac1_val_b16=0x0fff) # 4V
##pgu.dacx_test_data_write_32b(dac0_val_b16=0x2fff, dac1_val_b16=0x0fff) # 3V // noise BD1/2
#pgu.dacx_test_data_write_32b(dac0_val_b16=0x1fff, dac1_val_b16=0x0fff) # 2V // noise BD3
##pgu.dacx_test_data_write_32b(dac0_val_b16=0x0fff, dac1_val_b16=0x0fff) # 1V
#input('>>> Press Enter to Stop')
#pgu.dacx_test_data_write_32b(dac0_val_b16=0, dac1_val_b16=0)


# SLT (single level test)
pgu.dacx_slt_write_data (dac0_val_b16=0x0000, dac1_val_b16=0x0000)
[rb_dac0_val_b16, rb_dac1_val_b16]=pgu.dacx_slt_read_data()
print('{} = {}'.format('rb_dac0_val_b16',rb_dac0_val_b16))#
print('{} = {}'.format('rb_dac1_val_b16',rb_dac1_val_b16))#
pgu.dacx_slt_run_test()
input('>>> Press Enter to Next')
pgu.dacx_slt_stop_test()
pgu.dacx_slt_write_data (dac0_val_b16=0x1fff, dac1_val_b16=0x0fff) # 2V
[rb_dac0_val_b16, rb_dac1_val_b16]=pgu.dacx_slt_read_data()
print('{} = {}'.format('rb_dac0_val_b16',rb_dac0_val_b16))#
print('{} = {}'.format('rb_dac1_val_b16',rb_dac1_val_b16))#
pgu.dacx_slt_run_test()
input('>>> Press Enter to Stop')
pgu.dacx_slt_write_data (dac0_val_b16=0x0000, dac1_val_b16=0x0000)


##--------------------------------------------------##


####
## power control off
#
print('>> power off')
pgu.spio_ext__pwr_led(led=0,pwr_dac=0,pwr_adc=0,pwr_amp=0)
#

####
pgu.sleep(3)
####

####
## test counter off
ret = pgu.test_counter('OFF')
print(ret)
####

####
## test counter reset
ret = pgu.test_counter('RESET')
print(ret)
####


####
## close
ret = pgu.ok_close()
print(ret)
####
