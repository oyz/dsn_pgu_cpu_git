## LAN / TCP server based SCPI control for CMU-CPU
##   replaced ok endpoints with lan-mcs endpoints
## 
#
## LAN_cmu_cpu__lib.py : LAN access function library for CMU-CPU-TEST-F5500 

####
## library call
#
from sys import exit
#
import time
#
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
#
#import tkinter as tk
#from tkinter import filedialog
import os
#
import datetime
#
import csv
#
#
import socket ##$$ for LAN 
from socket import AF_INET, SOCK_DGRAM
#
####

####
## import configuration 
#  
#  load OK_EP_ADRS_CONFIG
import ok_cmu_cpu__lib_conf as conf
#  
####

####
## (0) test or common
#
# cmu parameter display/control class
class cmu_ctrl__class:
	dev = []
#
####


# sleep 
def sleep(val):
	return time.sleep(val)
#

####
## initialize control variable
cmu_ctrl = cmu_ctrl__class()
#
####


####$$$$

## LAN parameters 
HOST = '192.168.168.123'  # The server's hostname or IP address
HOST_REMOTE = '192.168.172.1'  # The server's hostname or IP address
PORT = 5025               # The port used by the server
TIMEOUT = 5.3 # socket timeout
SO_SNDBUF = 2048
SO_RCVBUF = 32768
INTVAL = 0.1 # sec for waiting before recv()
BUF_SIZE_NORMAL = 2048
BUF_SIZE_LARGE = 16384
TIMEOUT_LARGE = TIMEOUT*10
#
cmd_str__IDN = b'*IDN?\n'
cmd_str__RST = b'*RST\n'
cmd_str__CMEP_EN = b':CMEP:EN'
#
cmd_str__CMEP_MKWI = b':CMEP:MKWI'
cmd_str__CMEP_MKWO = b':CMEP:MKWO'
cmd_str__CMEP_MKTI = b':CMEP:MKTI'
cmd_str__CMEP_MKTO = b':CMEP:MKTO'
#
cmd_str__CMEP_WI = b':CMEP:WI'
cmd_str__CMEP_WO = b':CMEP:WO'
cmd_str__CMEP_TI = b':CMEP:TI'
cmd_str__CMEP_TO = b':CMEP:TO'
cmd_str__CMEP_TAC = b':CMEP:TAC'
cmd_str__CMEP_TMO = b':CMEP:TMO'
cmd_str__CMEP_PI = b':CMEP:PI'
cmd_str__CMEP_PO = b':CMEP:PO'
#
cmd_str__CMEP_WMI = b':CMEP:WMI'
cmd_str__CMEP_WMO = b':CMEP:WMO'


## LAN subfunctions
#
# scpi socket open
def scpi_open (timeout=TIMEOUT, so_sndbuf=SO_SNDBUF, so_rcvbuf=SO_RCVBUF):
	try:
		ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		ss.settimeout(timeout)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, so_sndbuf)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, so_rcvbuf) # 8192 16384 32768 65536
	except OSError as msg:
		ss = None
		print('error in socket')
		raise
	return ss
# scpi socket connect
def scpi_connect (ss, host=HOST, port=PORT):
	try:
		ss.connect((host, port))
	except socket.timeout as msg:
		print('error in connect:'+repr(msg))
		print(repr(host), repr(port))
		ss.close()
		ss = None
		print('socket closed')
		raise
		#input('Enter any key to try remote host: '+repr(HOST_REMOTE))
		#try:
		#	ss.close()
		#	ss = scpi_open()
		#	ss.connect((HOST_REMOTE, port))
		#except OSError as msg:
		#	print('error in connect:'+repr(msg))
		#	ss.close()
		#	ss = None
		#	raise 	
	except socket.error as msg:
		print('error in connect:'+repr(msg))
		raise
	return ss
# scpi socket close
def scpi_close (ss):
	try:
		ss.close()
	except:
		if ss == None:
			print('error: ss==None')
		raise

# send scpi command and get response : data must be ascii with a sentinel '\n'.
def scpi_comm_resp (ss, cmd_str, buf_size=BUF_SIZE_NORMAL, intval=INTVAL) :
	try:
		#print(cmd_str)
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# recv data until finding the sentinel '\n'
	try:
		data = ss.recv(buf_size) # try 1024 131072 524288
		# try   
		while (1):
			if len(data)==0:
				data = ss.recv(buf_size)
				continue
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
					break
			data = data + ss.recv(buf_size)
	except:
		print('error in recv')
		raise
	#
	##$$if (len(data)>20):
	##$$	print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	##$$else:
	##$$	print('Received:', repr(data))
	#
	return data
#
# send scpi command and get numerical block response : data may have binary parts.
# ...
def scpi_comm_resp_numb (ss, cmd_str, buf_size=BUF_SIZE_LARGE, intval=INTVAL, timeout_large=TIMEOUT_LARGE) :
	try:
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# cmd: ":CMEP:PO#HBC 524288\n"
	# rsp: "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
	#
	# recv data until finding the sentinel '\n' // TODO: numeric block
	# but check the sentinel after the data byte count is met.
	#
	# read timeout
	to = ss.gettimeout()
	#print(to)
	# increase timeout
	ss.settimeout(timeout_large)
	#
	try:
		# find the numeric head : must 10 in data 
		data = ss.recv(buf_size)
		while True:
			if len(data)>=10:
				break
			data = data + ss.recv(buf_size)
		#
		#print('header: ', repr(data[0:10])) # header
		#
		# find byte count 
		try:
			byte_count = int(data[3:9])
		except ValueError:
			print('error in byte_count : ValueError')
			print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
			raise
		except:
			print('error in byte_count')
			print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
			raise
		print('byte_count=', repr(byte_count)) 
		#
		# collect all data by byte count
		count_to_recv = byte_count + 10 + 1# add header count #add /n
		while True:
			if len(data)>=count_to_recv:
				break
			data = data + ss.recv(buf_size)
		#
		# check the sentinel 
		while True:
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
		#
	except:
		print('error in recv')
		raise
	#
	if (len(data)>20):
		print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
		print('Received:', repr(data[-20:-1]),  ' (last 20 bytes)')
	##$$else:
	##$$	print('Received:', repr(data))
	#
	# timeout back to prev
	ss.settimeout(to)
	#
	data = data[10:(10+byte_count)]
	print('data:', data[0:20].hex(),  ' (first 20 bytes)')
	#
	return [byte_count, data]
	
## device class for LAN-CMU-CPU // TODO: lib functions
class LAN_CMU_Dev:
	dev_count = 0
	idn = []
	ss = None # socket
	#
	f_scpi_open = scpi_open
	f_scpi_connect = scpi_connect
	f_scpi_close = scpi_close
	f_scpi_cmd = scpi_comm_resp
	f_scpi_cmd_numb = scpi_comm_resp_numb
	#
	def _test(self):
		return 'LAN_CMU_Dev'
	#
	def GetDeviceCount(self):
		# must update from ping ip ... or else 
		self.dev_count = 1
		return self.dev_count 
	#
	def Init(self):
		# nothing
		pass
	def Open(self, hh=HOST, pp=PORT):
		# open scpi
		self.ss = LAN_CMU_Dev.f_scpi_open()		
		# connect scpi
		LAN_CMU_Dev.f_scpi_connect(self.ss,hh,pp)
		#
		#if ret==None:
		#	# try remote host 
		#	input('Enter any key to try remote host: '+repr(HOST_REMOTE))
		#	self.ss = LAN_CMU_Dev.f_scpi_open()
		#	ret = LAN_CMU_Dev.f_scpi_connect(self.ss,HOST_REMOTE,pp)
		#
		# wait 
		#sleep(2)
		# board reset 
		ret = LAN_CMU_Dev.f_scpi_cmd(self.ss, cmd_str__RST).decode()
		#
		# LAN end-point control enable
		ret = LAN_CMU_Dev.f_scpi_cmd(self.ss, cmd_str__CMEP_EN+b' ON\n').decode()
		return ret
	def GetSerialNumber(self):
		ret = LAN_CMU_Dev.f_scpi_cmd(self.ss, cmd_str__IDN).decode()
		return ret # must come from board later
	def ConfigureFPGA(self, opt=[]):
		# not support
		pass
		return 0
	def GetErrorString(self, opt):
		# not support
		pass
		return []
	def Close(self):
		ret = LAN_CMU_Dev.f_scpi_cmd(self.ss, cmd_str__CMEP_EN+b' OFF\n').decode()
		# close scpi
		LAN_CMU_Dev.f_scpi_close(self.ss)
		self.ss = None
		return ret
	#
	def GetWireOutValue(self, adrs, mask=0xFFFFFFFF):
		## write wireout mask 
		# :CMEP:MKWO  #Hmmmmmmmm <NL>
		#
		#cmd_str = cmd_str__CMEP_MKWO + (' #H{:08X}\n'.format(mask)).encode()
		#rsp_str = LAN_CMU_Dev.f_scpi_cmd(self.ss, cmd_str)
		#
		## read wireout
		# :CMEP:WO#Hnn? <NL>
		#
		#cmd_str = cmd_str__CMEP_WO + ('#H{:02X}?\n'.format(adrs)).encode()
		#rsp_str = LAN_CMU_Dev.f_scpi_cmd(self.ss, cmd_str)
		
		## combined
		# :CMEP:WMO#Hnn  #Hmmmmmmmm<NL>
		cmd_str = cmd_str__CMEP_WMO + ('#H{:02X} #H{:08X}\n'.format(adrs,mask)).encode()
		rsp_str = LAN_CMU_Dev.f_scpi_cmd(self.ss, cmd_str)
		#
		rsp = rsp_str.decode()
		# assume hex decimal response: #HF3190306<NL>
		rsp = '0x' + rsp[2:-1] # convert "#HF3190306<NL>" --> "0xF3190306"
		rsp = int(rsp,16) # convert hex into int
		return rsp
	def UpdateWireOuts(self):
		# no global update : nothing to do.
		pass
	def SetWireInValue(self, adrs, data, mask=0xFFFFFFFF):
		## write wirein mask 
		# :CMEP:MKWI  #Hmmmmmmmm <NL>
		#
		#cmd_str = cmd_str__CMEP_MKWI + (' #H{:08X}\n'.format(mask)).encode()
		#rsp_str = LAN_CMU_Dev.f_scpi_cmd(self.ss, cmd_str)
		##$$print('Sent:', repr(cmd_str))
		##$$print('Rcvd:', repr(rsp_str))
		#
		## write wirein
		# :CMEP:WI#Hnn  #Hnnnnnnnn <NL>
		#
		#cmd_str = cmd_str__CMEP_WI + ('#H{:02X} #H{:08X}\n'.format(adrs,data)).encode()
		#rsp_str = LAN_CMU_Dev.f_scpi_cmd(self.ss, cmd_str)
		##$$print('Sent:', repr(cmd_str))
		##$$print('Rcvd:', repr(rsp_str))
		
		## combined
		# :CMEP:WMI#Hnn  #Hnnnnnnnn #Hmmmmmmmm<NL>
		cmd_str = cmd_str__CMEP_WMI + ('#H{:02X} #H{:08X} #H{:08X}\n'.format(adrs,data,mask)).encode()
		rsp_str = LAN_CMU_Dev.f_scpi_cmd(self.ss, cmd_str)
		#
		rsp = rsp_str.decode()
		return rsp
	def UpdateWireIns(self):
		# no global update : nothing to do.
		pass
	#
	def ActivateTriggerIn(self, adrs, loc_bit):
		## activate trig 
		# :CMEP:TAC#Hnn  #Hnn<NL>
		#
		cmd_str = cmd_str__CMEP_TAC + ('#H{:02X} #H{:02X}\n'.format(adrs,loc_bit)).encode()
		rsp_str = LAN_CMU_Dev.f_scpi_cmd(self.ss, cmd_str)
		rsp = rsp_str.decode()
		return rsp
	def IsTriggered (self, adrs, mask):
		# not used in CMU
		pass 
	#
	def ReadFromPipeOut(self, adrs, data_bytearray):
		## read pipeout
		# :CMEP:PO#Hnn nnnnnn<NL>
		#
		byte_count = len(data_bytearray)
		#
		cmd_str = cmd_str__CMEP_PO + ('#H{:02X} {:06d}\n'.format(adrs,byte_count)).encode()
		print(cmd_str)
		#
		[rsp_cnt, rsp_str] = LAN_CMU_Dev.f_scpi_cmd_numb(self.ss, cmd_str)
		#
		# assume numeric block : "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
		# assume rsp_str is data part.
		# copy data 
		for ii in range(0,rsp_cnt): 
			data_bytearray[ii] = rsp_str[ii]
		#
		return rsp_cnt
	def WriteToPipeIn(self, adrs, data_bytearray):
		# not used in CMU
		pass 
		
####
## (1) init OK device
def ok_cmu_init():
	##$$print('\n>> {}'.format('Call okCFrontPanel'))
	print('\n>> {}'.format('Call LAN SPCI protocol'))
	#
	##$$cmu_ctrl.dev = ok.okCFrontPanel()
	cmu_ctrl.dev = LAN_CMU_Dev()
	cmu_ctrl.dev.Init()
	#
	return cmu_ctrl.dev
#
def ok_cmu_caller_id():
	print('\n>> {}'.format('Return the previous caller in Class cmu_ctrl'))
	return cmu_ctrl.dev
#
####


####
## (2) open OK device
def ok_cmu_open(hh=HOST,pp=PORT):
	dev = cmu_ctrl.dev
	print('\n>> {}'.format('Enumerate devices'))
	deviceCount = dev.GetDeviceCount()
	print('Device Count: {}'.format(deviceCount))
	##$$for i in range(deviceCount):
	##$$	print('Device[{0}] Model: {1}'.format(i, dev.GetDeviceListModel(i)))
	##$$	print('Device[{0}] Serial: {1}'.format(i, dev.GetDeviceListSerial(i)))
	##$$if deviceCount == 0:
	##$$	#exit(0)
	##$$	return False
	##$$#
	##$$print('\n>> {}'.format('Open a device'))
	##$$dev.OpenBySerial("")
	print('\n>> {}'.format('Open a device: enable LAN SCPI protocol'))
	dev.Open(hh,pp)
	##$$DevID = dev.GetDeviceID()
	DevSR = dev.GetSerialNumber()
	##$$DevBoardModel = dev.GetBoardModel()
	##$$DevBoardModelStr = dev.GetBoardModelString(DevBoardModel)
	##$$#
	##$$print('{}: {}'.format('Opened Device ID',DevID))
	##$$print('{}: {}'.format('Opened Device Serial',DevSR))
	##$$print('{}: {}'.format('Opened Device Board Model String',DevBoardModelStr))
	##$$#
	cmu_ctrl.idn=DevSR
	return [deviceCount, DevSR]
	
#
####


####
## (3) close OK device
# 
def ok_cmu_close():
	dev = cmu_ctrl.dev
	print('\n>> {}'.format('Close a device: disable LAN SCPI protocol'))
	# disconnect and close
	return dev.Close()
#
####

####
## (4) configure FPGA
#
#
def ok_cmu_conf(BIT_FILENAME=[]):
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	if not BIT_FILENAME:
		BIT_FILENAME = EP_ADRS['bit_filename']
	#
	print('\n>> {}'.format('Configure FPGA'))
	print('{}: {}\n'.format('BIT filename',BIT_FILENAME))
	error = dev.ConfigureFPGA(BIT_FILENAME)
	if error !=0:
		print('Error {} : {}'.format(error,dev.GetErrorString(error)))
		#exit(0)
		return False
	#
	return BIT_FILENAME
#
#
def form_hex_32b(val):
	#return '{:#010X}'.format(val)
	return '0x{:08X}'.format(val)
#
def cmu_read_fpga_image_id():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('\n>> {}'.format('Read FPGA image ID'))
	#
	dev.UpdateWireOuts()
	fpga_image_id = dev.GetWireOutValue(EP_ADRS['FPGA_IMAGE_ID'])
	#
	print('{} = {:#10x}'.format('fpga_image_id',fpga_image_id))
	#
	fpga_image_id_expected = EP_ADRS['ver'] # hex string
	#
	print('{} = {}'.format('fpga_image_id_expected',fpga_image_id_expected))
	#
	if form_hex_32b(fpga_image_id) == fpga_image_id_expected:
		print('> fpga_image_id is matched as expected.')
	else:
		print('> fpga_image_id is NOT same as expected!')
	#
	return fpga_image_id
#
#
####


####
## (5) monitor FPGA
#
def cmu_monitor_fpga():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('\n>> {}'.format('Monitor FPGA'))
	#
	dev.UpdateWireOuts()
	mon_fpga_temp_mC = dev.GetWireOutValue(EP_ADRS['XADC_TEMP'])
	mon_fpga_volt_mV = dev.GetWireOutValue(EP_ADRS['XADC_VOLT'])
	#
	print('{}: {}\r'.format('FPGA temp[C]',mon_fpga_temp_mC/1000))
	print('{}: {}\r'.format('FPGA volt[V]',mon_fpga_volt_mV/1000))
	#
	return [mon_fpga_temp_mC, mon_fpga_volt_mV]
#
####



####
## (6) test counter
#
#assign reset1     = w_TEST_CON[0];
#assign disable1   = w_TEST_CON[1];
#assign autocount2 = w_TEST_CON[2];
#assign reset2     = w_TEST_TI[0];
#assign up2        = w_TEST_TI[1];
#assign down2      = w_TEST_TI[2];
#assign w_TEST_OUT = {16'b0, count2[7:0], count1[7:0]}; // TEST_OUT
#assign w_TEST_TO   = {15'b0, count2eqFF, 14'b0, count1eq80, count1eq00};
#
#  https://www.opalkelly.com/examples/counting
#
def cmu_test_counter(opt='OFF'):
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	if not opt:
		opt = 'OFF'
	#
	print('\n>> {}'.format('Test counter'))
	#
	# clear reset1
	dev.SetWireInValue(EP_ADRS['TEST_CON'],0x00,0x01) # (ep,val,mask)
	# clear disable1
	dev.SetWireInValue(EP_ADRS['TEST_CON'],0x00,0x02) # (ep,val,mask)
	#
	if opt.upper()=='ON':
		# set autocount2
		dev.SetWireInValue(EP_ADRS['TEST_CON'],0x04,0x04) # (ep,val,mask)
	elif opt.upper()=='OFF':
		# clear autocount2
		dev.SetWireInValue(EP_ADRS['TEST_CON'],0x00,0x04) # (ep,val,mask)
	else:
		pass
	#
	dev.UpdateWireIns()
	#
	#
	if opt.upper()=='RESET':
		# set reset2 // reset counter #2
		dev.ActivateTriggerIn(EP_ADRS['TEST_TI'], 0) # (ep,bit)
	else:
		pass
	#
	# read counters 
	dev.UpdateWireOuts()
	test_out = dev.GetWireOutValue(EP_ADRS['TEST_OUT'])
	count1 = test_out & 0xFF
	count2 = (test_out>>8) & 0xFF
	#
	return [count1, count2]
#
####

####
## (7) SPO control
#
#wire [31:0] w_SPO_CON    = ep07wire;
#wire [63:0] w_SPO_DIN_B0 = {ep09wire, ep08wire};
#wire [63:0] w_SPO_DIN_B1 = {ep0Bwire, ep0Awire};
#wire [63:0] w_SPO_DIN_B2 = {ep0Dwire, ep0Cwire};
#wire [63:0] w_SPO_DIN_B3 = {ep0Fwire, ep0Ewire};
#	assign ep28wire = w_SPO_MON_B0[31: 0];
#	assign ep29wire = w_SPO_MON_B0[63:32];
#	assign ep2Awire = w_SPO_MON_B1[31: 0];
#	assign ep2Bwire = w_SPO_MON_B1[63:32];
#	assign ep2Cwire = w_SPO_MON_B2[31: 0];
#	assign ep2Dwire = w_SPO_MON_B2[63:32];
#	assign ep2Ewire = w_SPO_MON_B3[31: 0];
#	assign ep2Fwire = w_SPO_MON_B3[63:32];
#assign ep27wire = w_SPO_FLAG;
#wire CTL_IO_CON_en					= w_SPO_CON[0];
#wire CTL_IO_CON_init 				= w_SPO_CON[1];
#wire CTL_IO_CON_update 			= w_SPO_CON[2];
#wire CTL_IO_CON_test 				= w_SPO_CON[3];
#wire [2:0]  CTL_IO_CON_adrs_start	= w_SPO_CON[6:4];
#wire [2:0]  CTL_IO_CON_num_bytes 	= w_SPO_CON[10:8];
#wire [13:0] CTL_IO_CON_test_pdata	= w_SPO_CON[29:16];
#
# enable
def cmu_spo_enable():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Enable SPO'))
	#
	wi = EP_ADRS['SPO_CON']
	wo = EP_ADRS['SPO_FLAG']
	#
	# set enable_bit
	dev.SetWireInValue(wi,0x00000001,0x00000001) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	# read flag 
	dev.UpdateWireOuts()
	flag = dev.GetWireOutValue(wo)
	#
	print('{} = {:#010x}'.format('flag',flag))
	#
	flag = (flag&0x00000001)
	if flag!=0x01:
		print('>>>> error: cmu_spo_enable')
		input('Press Enter key!')
	#
	return flag
#
# initialize 
def cmu_spo_init():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Initialize SPO'))
	#
	wi = EP_ADRS['SPO_CON']
	wo = EP_ADRS['SPO_FLAG']
	#
	# set parameters adrs_start: 0
	dev.SetWireInValue(wi,0x00<<4,0x07<<4) # (ep,val,mask) 
	dev.UpdateWireIns()
	#
	# set parameters num_bytes: 7
	dev.SetWireInValue(wi,0x07<<8,0x07<<8) # (ep,val,mask) 
	dev.UpdateWireIns()
	#
	# set bit
	dev.SetWireInValue(wi,0x00000002,0x00000002) # (ep,val,mask) 
	dev.UpdateWireIns()
	# reset bit
	dev.SetWireInValue(wi,0x00000000,0x00000002) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	# check init_done flag
	cnt_done = 0
	MAX_CNT = 20000
	while True:
		dev.UpdateWireOuts()
		flag = dev.GetWireOutValue(wo)
		print('{} = {:#010x}'.format('flag',flag))
		init_done = (flag&0x00000002)>>1
		print('{} = {:#010x}'.format('flag',flag))
		if (init_done==1):
			break
		cnt_done += 1
		if (cnt_done>=MAX_CNT):
			break
	#  
	print('{} = {}'.format('cnt_done',cnt_done))#
	print('{} = {}'.format('init_done',init_done))
	#
	return init_done
#
# update 
def cmu_spo_update():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Update SPO'))
	#
	wi = EP_ADRS['SPO_CON']
	wo = EP_ADRS['SPO_FLAG']
	#
	# set bit
	dev.SetWireInValue(wi,0x00000004,0x00000004) # (ep,val,mask) 
	dev.UpdateWireIns()
	# reset bit
	dev.SetWireInValue(wi,0x00000000,0x00000004) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	# check update_done flag
	cnt_done = 0
	MAX_CNT = 20000
	while True:
		dev.UpdateWireOuts()
		flag = dev.GetWireOutValue(wo)
		#print('{} = {:#010x}'.format('flag',flag))
		update_done = (flag&0x00000004)>>2
		if (update_done==1):
			break
		cnt_done += 1
		if (cnt_done>=MAX_CNT):
			break
	#  
	print('{} = {}'.format('cnt_done',cnt_done))#
	print('{} = {}'.format('update_done',update_done))
	#
	return update_done
#
# set SPO buffer 
def cmu_spo_set_buffer(spo_idx='',val=0,mask=0xFFFFFFFF):
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Set SPO buffer'))
	#
	try:
		ep = EP_ADRS[spo_idx]
	except:
		print('> spo_idx is expected as SPO_DIN_B#_L or SPO_DIN_B#_H.')
		return False
	#
	print('{} = 0x{:02X}'.format('ep',ep))
	print('{} = 0x{:08X}'.format('val',val))
	print('{} = 0x{:08X}'.format('mask',mask))
	#
	dev.SetWireInValue(ep,val,mask) 
	dev.UpdateWireIns()
	#
	return True
#
# read SPO buffer
def cmu_spo_read_buffer(spo_idx=''):
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Read SPO buffer'))
	#
	try:
		ep = EP_ADRS[spo_idx]
	except:
		print('> spo_idx is expected as SPO_MON_B#_L or SPO_MON_B#_H.')
		return False
	#
	dev.UpdateWireOuts()
	val = dev.GetWireOutValue(ep)
	#
	print('{} = 0x{:02X}'.format('ep',ep))
	#
	return val
#
# disable
def cmu_spo_disable():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Disable SPO'))
	#
	wi = EP_ADRS['SPO_CON']
	wo = EP_ADRS['SPO_FLAG']
	#
	# clear enable_bit
	dev.SetWireInValue(wi,0x00000000,0x00000001) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	# read flag 
	dev.UpdateWireOuts()
	flag = dev.GetWireOutValue(wo)
	#
	print('{} = {:#010x}'.format('flag',flag))
	#
	flag = (flag&0x00000001)
	#
	return flag
#
#
# Control LEDs
def cmu_spo_bit__leds(opt=0xFF):
	print('>>> {} : 0x{:02X}'.format('Control LEDs',opt))
	#
	try:
		val = (opt&0xFF)<<8
	except:
		print('> opt is expected as 0x00 ~ 0xFF.')
		return False
	mask = 0x0000FF00
	cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_update()
	return True
#
# Control AMP_PWR
def cmu_spo_bit__amp_pwr(opt='OFF'):
	print('>>> {} : {}'.format('Control AMP_PWR',opt))
	val_dict = {
		'OFF' : 0x00000000, 
		'ON'  : 0x00000001
	}
	try:
		val = val_dict[opt]
	except:
		print('> opt is expected as OFF or ON.')
		return False
	mask = 0x00000001
	cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_update()
	return True
#
# Control ADC gain
def cmu_spo_bit__adc0_gain(opt='1X'):
	print('>>> {} : {}'.format('Control ADC0 gain',opt))
	val_dict = {
		'100X' : 0x00020000, 
		'10X'  : 0x00010000, 
		'1X'   : 0x00000000,
		'OFF'  : 0x00000000
	}
	try:
		val = val_dict[opt]
	except:
		print('> opt is expected as 1X, 10X or 100X.')
		return False
	mask = 0x000F0000
	cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_update()
	return True
#
def cmu_spo_bit__adc1_gain(opt='1X'):
	print('>>> {} : {}'.format('Control ADC1 gain',opt))
	val_dict = {
		'100X' : 0x00200000, 
		'10X'  : 0x00100000, 
		'1X'   : 0x00000000,
		'OFF'  : 0x00000000
	}
	try:
		val = val_dict[opt]
	except:
		print('> opt is expected as 1X, 10X or 100X.')
		return False
	mask = 0x00F00000
	cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_update()
	return True
#
# Control TEST-out filter path
def cmu_spo_bit__vi_bw(opt='1M2'):
	print('>>> {} : {}'.format('Control vi bandwidth',opt))
	val_dict = {
		'120K' : 0x08000000, 
		'1M2'  : 0x04000000, 
		'12M'  : 0x02000000, 
		'120M' : 0x01000000, 
		'OFF'  : 0x00000000
	}
	try:
		val = val_dict[opt]
	except:
		print('> opt is expected as 1M2, 120K or ...')
		return False
	mask = 0x0F000000
	cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_update()
	return True
#
def cmu_spo_bit__vq_bw(opt='1M2'):
	print('>>> {} : {}'.format('Control vq bandwidth',opt))
	val_dict = {
		'120K' : 0x80000000, 
		'1M2'  : 0x40000000, 
		'12M'  : 0x20000000, 
		'120M' : 0x10000000, 
		'OFF'  : 0x00000000
	}
	try:
		val = val_dict[opt]
	except:
		print('> opt is expected as 1M2, 120K or ...')
		return False
	mask = 0xF0000000
	cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_update()
	return True
#
#
####


####
## (8) DAC control
#
#wire [31:0] w_DAC_BIAS_CON   = ep11wire;
#wire [31:0] w_DAC_BIAS_DIN21 = ep16wire;
#wire [31:0] w_DAC_BIAS_DIN43 = ep17wire;
#	assign ep31wire = w_DAC_BIAS_FLAG;
#	assign ep36wire = w_DAC_BIAS_RB21;
#	assign ep37wire = w_DAC_BIAS_RB43;
#	
#wire [31:0] w_DAC_A2A3_CON   = ep10wire;
#wire [31:0] w_DAC_A2A3_DIN21 = ep14wire;
#wire [31:0] w_DAC_A2A3_DIN43 = ep15wire;
#	assign ep30wire = w_DAC_A2A3_FLAG;
#	assign ep34wire = w_DAC_A2A3_RB21;
#	assign ep35wire = w_DAC_A2A3_RB43;
#
#
# enable
def cmu_dac_bias_enable():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Enable DAC_BIAS'))
	#
	wi = EP_ADRS['DAC_BIAS_CON']
	wo = EP_ADRS['DAC_BIAS_FLAG']
	#
	dev.SetWireInValue(wi,0x00000001,0x00000001) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	dev.UpdateWireOuts()
	flag = dev.GetWireOutValue(wo)
	print('{} = {:#010x}'.format('flag',flag))
	#
	flag = (flag&0x00000001)
	if flag!=0x01:
		print('>>>> error: cmu_dac_bias_enable')
		input('Press Enter key!')
	#
	return flag
#
# initialize 
def cmu_dac_bias_init():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Initialize DAC_BIAS'))
	#
	wi = EP_ADRS['DAC_BIAS_CON']
	wo = EP_ADRS['DAC_BIAS_FLAG']
	#
	# set parameters ... forced pin output : 0x0B
	val = 0x0B
	dev.SetWireInValue(wi,val<<16,0xFF<<16) # (ep,val,mask) 
	dev.UpdateWireIns()
	#
	# set parameters ... range : 0xFF for +/-10V; 0xAA for +/-5V.
	val = 0xFF
	dev.SetWireInValue(wi,val<<24,0xFF<<24) # (ep,val,mask) 
	dev.UpdateWireIns()
	#
	# set bit
	dev.SetWireInValue(wi,0x00000002,0x00000002) # (ep,val,mask) 
	dev.UpdateWireIns()
	# reset bit
	dev.SetWireInValue(wi,0x00000000,0x00000002) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	# check init_done flag
	cnt_done = 0
	MAX_CNT = 20000
	while True:
		dev.UpdateWireOuts()
		flag = dev.GetWireOutValue(wo)
		init_done = (flag&0x00000002)>>1
		print('{} = {:#010x}'.format('flag',flag))
		if (init_done==1):
			break
		cnt_done += 1
		if (cnt_done>=MAX_CNT):
			break
	#  
	print('{} = {}'.format('cnt_done',cnt_done))#
	print('{} = {}'.format('init_done',init_done))
	#
	return init_done
#
# update 
def cmu_dac_bias_update():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Update DAC_BIAS'))
	#
	wi = EP_ADRS['DAC_BIAS_CON']
	wo = EP_ADRS['DAC_BIAS_FLAG']
	#
	# set bit
	dev.SetWireInValue(wi,0x00000004,0x00000004) # (ep,val,mask) 
	dev.UpdateWireIns()
	# reset bit
	dev.SetWireInValue(wi,0x00000000,0x00000004) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	# check update_done flag
	cnt_done = 0
	MAX_CNT = 20000
	while True:
		dev.UpdateWireOuts()
		flag = dev.GetWireOutValue(wo)
		#print('{} = {:#010x}'.format('flag',flag))
		update_done = (flag&0x00000004)>>2
		if (update_done==1):
			break
		cnt_done += 1
		if (cnt_done>=MAX_CNT):
			break
	#  
	print('{} = {}'.format('cnt_done',cnt_done))#
	print('{} = {}'.format('update_done',update_done))
	#
	return update_done
#
# set buffer
def cmu_dac_bias_set_buffer(DAC1=0,DAC2=0,DAC3=0,DAC4=0):
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Set DAC_BIAS buffer'))
	#
	print('{} = 0x{:04X}'.format('DAC1',DAC1))
	print('{} = 0x{:04X}'.format('DAC2',DAC2))
	print('{} = 0x{:04X}'.format('DAC3',DAC3))
	print('{} = 0x{:04X}'.format('DAC4',DAC4))
	#
	wi21 = EP_ADRS['DAC_BIAS_DIN21']
	wi43 = EP_ADRS['DAC_BIAS_DIN43']
	#
	DAC_21 = (DAC2<<16)|DAC1
	DAC_43 = (DAC4<<16)|DAC3
	print('{}: 0x{:08x}'.format('DAC_21',DAC_21))
	print('{}: 0x{:08x}'.format('DAC_43',DAC_43))
	#
	dev.SetWireInValue(wi21,DAC_21,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(wi43,DAC_43,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	return [DAC_43, DAC_21]
#
# readback
def cmu_dac_bias_readback():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Readback DAC_BIAS'))
	#
	wo21 = EP_ADRS['DAC_BIAS_RB21']
	wo43 = EP_ADRS['DAC_BIAS_RB43']
	#
	dev.UpdateWireOuts()
	DAC_21 = dev.GetWireOutValue(wo21)
	DAC_43 = dev.GetWireOutValue(wo43)
	#
	DAC1 = (DAC_21    )&0xFFFF
	DAC2 = (DAC_21>>16)&0xFFFF
	DAC3 = (DAC_43    )&0xFFFF
	DAC4 = (DAC_43>>16)&0xFFFF
	print('{}: 0x{:04x}'.format('DAC1',DAC1))
	print('{}: 0x{:04x}'.format('DAC2',DAC2))
	print('{}: 0x{:04x}'.format('DAC3',DAC3))
	print('{}: 0x{:04x}'.format('DAC4',DAC4))
	#
	#
	return [DAC_43, DAC_21]
#
#
# disable
def cmu_dac_bias_disable():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Disable DAC_BIAS'))
	#
	wi = EP_ADRS['DAC_BIAS_CON']
	wo = EP_ADRS['DAC_BIAS_FLAG']
	#
	dev.SetWireInValue(wi,0x00000000,0x00000001) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	dev.UpdateWireOuts()
	flag = dev.GetWireOutValue(wo)
	print('{} = {:#010x}'.format('flag',flag))
	#
	flag = (flag&0x00000001)
	#
	return flag
#
# 
# 
####


####
## (9) DWAVE control
#
#
#wire [31:0] w_DATA_IN_BY_TRIG  = ep05wire;
#wire [31:0] w_DWAVE_CON        = ep06wire;
#	assign ep25wire = w_DATA_OUT_BY_TRIG;
#	assign ep26wire = w_DWAVE_FLAG;
#	assign ep23wire = w_DWAVE_BASE_FREQ;
#
#wire dwave_en 						= w_DWAVE_CON[0];
#wire dwave_init 					= w_DWAVE_CON[1];
#wire dwave_update 					= w_DWAVE_CON[2];
#wire dwave_test 					= w_DWAVE_CON[3];
#wire [1:0] dwave_test_datain_type	= w_DWAVE_CON[5:4];
#wire [1:0] dwave_test_datain_port	= w_DWAVE_CON[7:6];
#wire [15:0] dwave_test_datain		= w_DWAVE_CON[31:16];
#
#wire [31:0] w_DWAVE_TI         = ep46trig;
#.i_trig_pulse_off      (w_DWAVE_TI[0]),
#.i_trig_pulse_on_cont  (w_DWAVE_TI[1]),
#.i_trig_pulse_on_num   (w_DWAVE_TI[2]),
#.i_trig_set_parameters (w_DWAVE_TI[3]),
#.i_trig_wr_cnt_period  (w_DWAVE_TI[16]),
#.i_trig_rd_cnt_period  (w_DWAVE_TI[24]),
#.i_trig_wr_cnt_diff    (w_DWAVE_TI[17]),
#.i_trig_rd_cnt_diff    (w_DWAVE_TI[25]),
#.i_trig_wr_num_pulses  (w_DWAVE_TI[18]),
#.i_trig_rd_num_pulses  (w_DWAVE_TI[26]),
#.i_trig_wr_output_dis  (w_DWAVE_TI[19]), 
#.i_trig_rd_output_dis  (w_DWAVE_TI[27]),
#// i_phase disable // q_phase disable  
#//   f1, f2 ... i_phase ... for disable ... 0x3 ..3
#//   f3, f4 ... q_phase ... for disable ... 0xC ..12
#
# enable
def cmu_dwave_enable():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Enable DWAVE'))
	#
	wi = EP_ADRS['DWAVE_CON']
	wo = EP_ADRS['DWAVE_FLAG']
	#
	dev.SetWireInValue(wi,0x00000001,0x00000001) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	dev.UpdateWireOuts()
	flag = dev.GetWireOutValue(wo)
	print('{} = {:#010x}'.format('flag',flag))
	#
	flag = (flag&0x00000001)
	if flag!=0x01:
		print('>>>> error: cmu_dwave_enable')
		input('Press Enter key!')
	#
	return flag
#
# read dwave base freq
def cmu_dwave_read_base_freq():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Read DWAVE_BASE_FREQ'))
	#
	wo = EP_ADRS['DWAVE_BASE_FREQ']
	#
	dev.UpdateWireOuts()
	dwave_base_freq = dev.GetWireOutValue(wo)
	#
	print('{}: 0x{:08x}'.format('dwave_base_freq',dwave_base_freq))
	#
	return dwave_base_freq
#
# trig read
def cmu_dwave_rd__trig(bit_loc=0):
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {} [{}]'.format('Trigger read DWAVE',bit_loc))
	#
	ti = EP_ADRS['DWAVE_TI']
	wo = EP_ADRS['DWAVE_DOUT_BY_TRIG']
	#
	# trig
	ret = dev.ActivateTriggerIn(ti, bit_loc) # (ep,bit) #
	#
	dev.UpdateWireOuts()
	val = dev.GetWireOutValue(wo)
	#
	return val
#
##TODO: def cmu_dwave_wr__trig()
def cmu_dwave_wr__trig(bit_loc=0, val=0):
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {} [{}] : {}'.format('Trigger write DWAVE',bit_loc,val))
	#
	ti = EP_ADRS['DWAVE_TI']
	wi = EP_ADRS['DWAVE_DIN_BY_TRIG']
	#
	dev.SetWireInValue(wi,val,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	# trig
	ret = dev.ActivateTriggerIn(ti, bit_loc) # (ep,bit) #
	#
	return val
#
# dwave_wr_cnt_period
def cmu_dwave_wr_cnt_period(val):
	print('>>> {} : {}'.format('Write DWAVE cnt_period',val))
	cmu_dwave_wr__trig(16, val)
	return True
#
# dwave_rd_cnt_period
def cmu_dwave_rd_cnt_period():
	print('>>> {}'.format('Read DWAVE cnt_period'))
	val = cmu_dwave_rd__trig(24)
	return val
#
# dwave_wr_cnt_diff
def cmu_dwave_wr_cnt_diff(val):
	print('>>> {} : {}'.format('Write DWAVE cnt_diff',val))
	cmu_dwave_wr__trig(17, val)
	return True
#
# dwave_rd_cnt_diff
def cmu_dwave_rd_cnt_diff():
	print('>>> {}'.format('Read DWAVE cnt_diff'))
	val = cmu_dwave_rd__trig(25)
	return val
#
# dwave_wr_num_pulses
def cmu_dwave_wr_num_pulses(val):
	print('>>> {} : {}'.format('Write DWAVE num_pulses',val))
	cmu_dwave_wr__trig(18, val)
	return True
#
# dwave_rd_num_pulses
def cmu_dwave_rd_num_pulses():
	print('>>> {}'.format('Read DWAVE num_pulses'))
	val = cmu_dwave_rd__trig(26)
	return val
#
#
# set dwave parameters
def cmu_dwave_set_para():
	print('>>> {}'.format('Set DWAVE parameters'))
	cmu_dwave_wr__trig(3, val=0)
	return True
#
#
# write output_dis bits 
def cmu_dwave_wr_output_dis(val):
	print('>>> {} : {}'.format('Write DWAVE output_dis',val))
	cmu_dwave_wr__trig(19, val)
	return True
#
# read output_dis bits 
def cmu_dwave_rd_output_dis():
	print('>>> {}'.format('Read DWAVE output_dis'))
	val = cmu_dwave_rd__trig(27)
	return val
#
def cmu_dwave_wr_output_dis__enable_all():
	print('>>>> {}'.format('Enable DWAVE all output'))
	cmu_dwave_wr_output_dis(0x0)
	return
#
def cmu_dwave_wr_output_dis__enable_path_q_only():
	print('>>>> {}'.format('Enable DWAVE only Q output'))
	cmu_dwave_wr_output_dis(0x3)
	return
#
def cmu_dwave_wr_output_dis__enable_path_i_only():
	print('>>>> {}'.format('Enable DWAVE only I output'))
	cmu_dwave_wr_output_dis(0xC)
	return
#
def cmu_dwave_wr_output_dis__disable_all():
	print('>>>> {}'.format('Disable DWAVE all output'))
	cmu_dwave_wr_output_dis(0xF)
	return
#
#
#
# dwave_pulse_on_num
def cmu_dwave_pulse_on_num():
	print('>>> {}'.format('Set DWAVE pulse_on_num'))
	cmu_dwave_wr__trig(2, val=0)
	return True
#
# dwave_pulse_on
def cmu_dwave_pulse_on():
	print('>>> {}'.format('Set DWAVE pulse_on'))
	cmu_dwave_wr__trig(1, val=0)
	return True
#
# dwave_pulse_off
def cmu_dwave_pulse_off():
	print('>>> {}'.format('Set DWAVE pulse_off'))
	cmu_dwave_wr__trig(0, val=0)
	return True
#
#
# disable
def cmu_dwave_disable():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Disable DWAVE'))
	#
	wi = EP_ADRS['DWAVE_CON']
	wo = EP_ADRS['DWAVE_FLAG']
	#
	dev.SetWireInValue(wi,0x00000000,0x00000001) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	dev.UpdateWireOuts()
	flag = dev.GetWireOutValue(wo)
	print('{} = {:#010x}'.format('flag',flag))
	#
	flag = (flag&0x00000001)
	#
	return flag
#
####


####
## (10) ADC control
#  
#  cmu_adc_enable
def cmu_adc_enable():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Enable ADC'))
	#
	wi = EP_ADRS['ADC_HS_WI']
	wo = EP_ADRS['ADC_HS_WO']
	#
	dev.SetWireInValue(wi,0x00000001,0x00000001) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	dev.UpdateWireOuts()
	flag = dev.GetWireOutValue(wo)
	print('{} = {:#010x}'.format('flag',flag))
	#
	flag = (flag&0x00000001)
	if flag!=0x01:
		print('>>>> error: cmu_adc_enable')
		input('Press Enter key!')
	#
	return flag
#
##TODO: def cmu_adc_read_base_freq()
def cmu_adc_read_base_freq():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Read ADC_BASE_FREQ'))
	#
	wo = EP_ADRS['ADC_BASE_FREQ']
	#
	dev.UpdateWireOuts()
	ADC_BASE_FREQ = dev.GetWireOutValue(wo)
	#
	print('{}: 0x{:08x}'.format('ADC_BASE_FREQ',ADC_BASE_FREQ))
	#
	return ADC_BASE_FREQ
#
##TODO: def cmu_adc_write_wire_endpoint_dict(idx_str, val, msk)
def cmu_adc_write_wire_endpoint_dict(idx_str, val, msk=0xFFFFFFFF):
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Write endpoint'))
	#
	wi = EP_ADRS[idx_str]
	#
	dev.SetWireInValue(wi,val,msk) # (ep,val,mask)
	dev.UpdateWireIns() # option
	#
	return wi
#
#  cmu_adc_reset
def cmu_adc_reset():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Reset ADC'))
	#
	ti = EP_ADRS['ADC_HS_TI']
	wo = EP_ADRS['ADC_HS_WO']
	#
	# trig
	bit_loc = 0 # for reset
	ret = dev.ActivateTriggerIn(ti, bit_loc) # (ep,bit) #
	#
	# flag
	dev.UpdateWireOuts()
	flag = dev.GetWireOutValue(wo)
	print('{} = {:#010x}'.format('flag',flag))
	#
	flag = (flag&0x0000000F)
	if flag!=0x01:
		print('>>>> error: cmu_adc_reset')
		input('Press Enter key!')
	#
	return flag
#  
##TODO: def cmu_adc_set_para()
def cmu_adc_set_para (
	ADC_BASE_FREQ=125000000,FS_TARGET=10416666,ADC_NUM_SAMPLES=131072,
	ADC_INPUT_DELAY_TAP=10,PIN_TEST_FRC_HIGH=0,PIN_DLLN_FRC_LOW=0,PTTN_CNT_UP_EN=0
	):
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Set ADC parameters'))
	#
	# read ADC base frequency @ ADC_BASE_FREQ
	print('> {}'.format('Read adc base freq'))
	dev.UpdateWireOuts()
	adc_base_freq = dev.GetWireOutValue(EP_ADRS['ADC_BASE_FREQ'])
	print('{}: {:#8.3f} MHz\r'.format('ADC Base Freq',adc_base_freq/1e6))
	if (ADC_BASE_FREQ!=adc_base_freq):
		print('>>> {}: {}'.format('Warning!: ADC Base Freq is not matched',adc_base_freq))
	#
	# calculate sampling frequency : Fs from adc_base_freq and FS_TARGET
	if adc_base_freq==0:
		print('>>> {}: {}'.format('Warning!: adc_base_freq is not accepted', 'ADC_BASE_FREQ is used.'))
		adc_base_freq = ADC_BASE_FREQ
	#
	ADC_CNT_SAMPLE_PERIOD_float = adc_base_freq/FS_TARGET
	ADC_CNT_SAMPLE_PERIOD = int(ADC_CNT_SAMPLE_PERIOD_float + 0.5) # round-off
	#
	if ADC_CNT_SAMPLE_PERIOD==0:
		print('>>> {}: {}'.format('Warning!: ADC_CNT_SAMPLE_PERIOD is not accepted', '1 is assigned.'))
		ADC_CNT_SAMPLE_PERIOD = 1
	#
	Fs = adc_base_freq/ADC_CNT_SAMPLE_PERIOD
	#
	print('{}: {:#8.3f} Sps\r'.format('FS_TARGET',FS_TARGET))
	print('{}: {:#8.3f} Counts\r'.format('ADC_CNT_SAMPLE_PERIOD_float',ADC_CNT_SAMPLE_PERIOD_float))
	print('{}: {:#8.3f} Counts\r'.format('ADC_CNT_SAMPLE_PERIOD',ADC_CNT_SAMPLE_PERIOD))
	print('{}: {:#8.3f} Msps\r'.format('Fs',Fs/1e6))	
	#
	# set sampling period count @ ADC_HS_SMP_PRD : ADC_CNT_SAMPLE_PERIOD (> 11)
	#   (125 megahertz) / 12 = 10.4166667 megahertz // best
	MIN_ADC_CNT_SAMPLE_PERIOD = 12
	if (ADC_CNT_SAMPLE_PERIOD < MIN_ADC_CNT_SAMPLE_PERIOD):
		print('>>> {}'.format('Warning!: ADC_CNT_SAMPLE_PERIOD is below minimun.'))
	dev.SetWireInValue(EP_ADRS['ADC_HS_SMP_PRD'],ADC_CNT_SAMPLE_PERIOD,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns() # option
	#
	# set adc data length to read @ wi1D : ADC_NUM_SAMPLES <= 2^17 = 131072 max 
	MAX_ADC_NUM_SAMPLES = 2**17
	if (ADC_NUM_SAMPLES > MAX_ADC_NUM_SAMPLES):
		print('>>> {}'.format('Warning!: ADC_NUM_SAMPLES is above maximum.'))
	dev.SetWireInValue(EP_ADRS['ADC_HS_UPD_SMP'],ADC_NUM_SAMPLES,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns() # must in final setting	#
	#
	# //$$ TODO: ADC_INPUT_DELAY_TAP rev
	# set input delay taps @ ADC_HS_DLY_TAP_OPT[31:27], ADC_HS_DLY_TAP_OPT[26:22]
	#ADC_INPUT_DELAY_TAP = 31
	print('{}: {:#8.3f} \r'.format('ADC_INPUT_DELAY_TAP',ADC_INPUT_DELAY_TAP))
	#
	#ADC_INPUT_DELAY_TAP_code = (ADC_INPUT_DELAY_TAP<<27)|(ADC_INPUT_DELAY_TAP<<22)
	#ADC_INPUT_DELAY_TAP_code = (ADC1_INPUT_DELAY_TAP_H<<27)|(ADC1_INPUT_DELAY_TAP_L<<22)|(ADC0_INPUT_DELAY_TAP_H<<17)|(ADC0_INPUT_DELAY_TAP_L<<12)
	ADC_INPUT_DELAY_TAP_code = (ADC_INPUT_DELAY_TAP<<27)|(ADC_INPUT_DELAY_TAP<<22)|(ADC_INPUT_DELAY_TAP<<17)|(ADC_INPUT_DELAY_TAP<<12)
	#
	print('{} = {:#010x}'.format('ADC_INPUT_DELAY_TAP_code',ADC_INPUT_DELAY_TAP_code))#
	#dev.SetWireInValue(EP_ADRS['ADC_HS_DLY_TAP_OPT'],ADC_INPUT_DELAY_TAP_code,0xFFC00000) # (ep,val,mask)
	dev.SetWireInValue(EP_ADRS['ADC_HS_DLY_TAP_OPT'],ADC_INPUT_DELAY_TAP_code,0xFFFFF000) # (ep,val,mask)
	#
	dev.UpdateWireIns() # must in final setting
	#
	# set w_pin_test_frc_high @ ADC_HS_DLY_TAP_OPT[0]
	# set w_pin_dlln_frc_low  @ ADC_HS_DLY_TAP_OPT[1]
	# set w_pttn_cnt_up_en    @ ADC_HS_DLY_TAP_OPT[2]
	ADC_control_code = (PTTN_CNT_UP_EN<<2)|(PIN_DLLN_FRC_LOW<<1)|PIN_TEST_FRC_HIGH
	print('{} = {:#010x}'.format('ADC_control_code',ADC_control_code))#
	dev.SetWireInValue(EP_ADRS['ADC_HS_DLY_TAP_OPT'],ADC_control_code,0x00000007) # (ep,val,mask)
	dev.UpdateWireIns() # must in final setting
	#	
	return True
#  
#  cmu_adc_init
def cmu_adc_init():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Initialize ADC'))
	#
	wi = EP_ADRS['ADC_HS_WI']
	ti = EP_ADRS['ADC_HS_TI']
	wo = EP_ADRS['ADC_HS_WO']
	#
	# trig
	dev.ActivateTriggerIn(ti, 1) # (ep,bit)
	#
	# check init_done flag
	cnt_done = 0
	MAX_CNT = 20000
	while True:
		dev.UpdateWireOuts()
		flag = dev.GetWireOutValue(wo)
		init_done = (flag&0x00000002)>>1
		print('{} = {:#010x}'.format('flag',flag))
		if (init_done==1):
			break
		cnt_done += 1
		if (cnt_done>=MAX_CNT):
			break
	#  
	print('{} = {}'.format('cnt_done',cnt_done))#
	print('{} = {}'.format('init_done',init_done))
	#
	return init_done
#  
#  cmu_check_adc_test_pattern
def cmu_check_adc_test_pattern (ADC_TEST_PATTERN=0x000330FC):
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Check ADC test pattern'))
	#
	wi = EP_ADRS['ADC_HS_WI']
	ti = EP_ADRS['ADC_HS_TI']
	wo = EP_ADRS['ADC_HS_WO']
	#
	po_d0 = EP_ADRS['ADC_HS_DOUT0_PO']
	po_d1 = EP_ADRS['ADC_HS_DOUT1_PO']
	#
	# read FIFO data generated by ADC initialization
	print('> {}'.format('Read data from FIFO'))
	#
	ADC_NUM_SAMPLES = 4 
	#
	dataout0  = bytearray([0] * ADC_NUM_SAMPLES*4) # 4 word = 4 * 4 byte 
	dataout1  = bytearray([0] * ADC_NUM_SAMPLES*4) # 4 word = 4 * 4 byte 
	#
	# TODO: ReadFromPipeOut()
	data_count0 = dev.ReadFromPipeOut(po_d0, dataout0)
	data_count1 = dev.ReadFromPipeOut(po_d1, dataout1)
	#
	print('{}: {}'.format('data_count0 [byte]',data_count0))
	print('{}: {}'.format('data_count1 [byte]',data_count1))
	#
	# convert 32-bit data into 18-bit adc_code
	adc0_list_int = []
	adc1_list_int = []
	for ii in range(0,ADC_NUM_SAMPLES):
		# note unsigned shift in python ... see int.from_bytes(..., signed=True) for signed shift
		temp_data0 = int.from_bytes(dataout0[ii*4:ii*4+4], byteorder='little', signed=True)
		temp_data1 = int.from_bytes(dataout1[ii*4:ii*4+4], byteorder='little', signed=True)
		adc0_list_int += [temp_data0>>14]
		adc1_list_int += [temp_data1>>14]
	#
	print('{}: {:#010x}'.format('adc0_list_int[0]&0x3FFFF',adc0_list_int[0]&0x3FFFF))
	print('{}: {:#010x}'.format('adc0_list_int[1]&0x3FFFF',adc0_list_int[1]&0x3FFFF))
	print('{}: {:#010x}'.format('adc0_list_int[2]&0x3FFFF',adc0_list_int[2]&0x3FFFF))
	print('{}: {:#010x}'.format('adc0_list_int[3]&0x3FFFF',adc0_list_int[3]&0x3FFFF))
	print('{}: {:#010x}'.format('adc1_list_int[0]&0x3FFFF',adc1_list_int[0]&0x3FFFF))
	print('{}: {:#010x}'.format('adc1_list_int[1]&0x3FFFF',adc1_list_int[1]&0x3FFFF))
	print('{}: {:#010x}'.format('adc1_list_int[2]&0x3FFFF',adc1_list_int[2]&0x3FFFF))
	print('{}: {:#010x}'.format('adc1_list_int[3]&0x3FFFF',adc1_list_int[3]&0x3FFFF))	
	#
	# check adc data by initialization (4 data of 0x330FC) // 209148 = 0x330FC
	chk_adcX_count_mismatch_pattern = 0
	for ii in range(0,ADC_NUM_SAMPLES):
		chk_adcX_count_mismatch_pattern += int((adc0_list_int[ii])&0x0003FFFF!=ADC_TEST_PATTERN)
		chk_adcX_count_mismatch_pattern += int((adc1_list_int[ii])&0x0003FFFF!=ADC_TEST_PATTERN)
	print('{} = {}'.format('chk_adcX_count_mismatch_pattern',chk_adcX_count_mismatch_pattern))#
	#
	ret = True
	#
	if (chk_adcX_count_mismatch_pattern!=0):
		print('>>> {}'.format('Warning!: adc test patterns are mismatched. need to check HW.'))
		ret = False	
	#
	return ret
#  
#  cmu_adc_update
def cmu_adc_update():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Update ADC samples'))
	#
	wi = EP_ADRS['ADC_HS_WI']
	ti = EP_ADRS['ADC_HS_TI']
	wo = EP_ADRS['ADC_HS_WO']
	to = EP_ADRS['ADC_HS_TO']
	#	
	# trig
	dev.ActivateTriggerIn(ti, 2) # (ep,bit)
	#
	# check update_done flag
	cnt_done = 0
	MAX_CNT = 20000
	while True:
		dev.UpdateWireOuts()
		flag = dev.GetWireOutValue(wo)
		update_done = (flag&0x00000004)>>2
		#print('{} = {:#010x}'.format('flag',flag))
		if (update_done==1):
			break
		cnt_done += 1
		if (cnt_done>=MAX_CNT):
			break
	#  
	print('{} = {}'.format('cnt_done',cnt_done))#
	print('{} = {}'.format('update_done',update_done))
	#
	return update_done	#
#  
#  cmu_adc_load_from_fifo
def cmu_adc_load_from_fifo (ADC_NUM_SAMPLES=131072): 
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Load ADC data into memory'))
	#
	wi = EP_ADRS['ADC_HS_WI']
	ti = EP_ADRS['ADC_HS_TI']
	wo = EP_ADRS['ADC_HS_WO']
	#
	po_d0 = EP_ADRS['ADC_HS_DOUT0_PO']
	po_d1 = EP_ADRS['ADC_HS_DOUT1_PO']
	#
	# init byte array
	dataout0  = bytearray([0] * ADC_NUM_SAMPLES*4) # ADC_NUM_SAMPLES word = ADC_NUM_SAMPLES * 4 byte 
	dataout1  = bytearray([0] * ADC_NUM_SAMPLES*4) # ADC_NUM_SAMPLES word = ADC_NUM_SAMPLES * 4 byte 
	#
	data_count0 = dev.ReadFromPipeOut(po_d0, dataout0)
	data_count1 = dev.ReadFromPipeOut(po_d1, dataout1)
	#
	print('{}: {}'.format('data_count0 [byte]',data_count0))
	print('{}: {}'.format('data_count1 [byte]',data_count1))
	#
	# convert 32-bit data into 18-bit adc_code
	adc0_list_int = []
	adc1_list_int = []
	for ii in range(0,ADC_NUM_SAMPLES):
		# note unsigned shift in python ... see int.from_bytes(..., signed=True) for signed shift
		temp_data0 = int.from_bytes(dataout0[ii*4:ii*4+4], byteorder='little', signed=True)
		temp_data1 = int.from_bytes(dataout1[ii*4:ii*4+4], byteorder='little', signed=True)
		adc0_list_int += [temp_data0>>14]
		adc1_list_int += [temp_data1>>14]
	#
	return [adc0_list_int, adc1_list_int]
#  
#  cmu_adc_disable
def cmu_adc_disable():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Disable ADC'))
	#
	wi = EP_ADRS['ADC_HS_WI']
	wo = EP_ADRS['ADC_HS_WO']
	#
	dev.SetWireInValue(wi,0x00000000,0x00000001) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	dev.UpdateWireOuts()
	flag = dev.GetWireOutValue(wo)
	print('{} = {:#010x}'.format('flag',flag))
	#
	flag = (flag&0x00000001)
	#
	return flag
#
#  cmu_adc_is_fifo_empty
def cmu_adc_is_fifo_empty():
	dev = cmu_ctrl.dev
	EP_ADRS = conf.OK_EP_ADRS_CONFIG
	#
	print('>> {}'.format('Is FIFO empty?'))
	#
	wi = EP_ADRS['ADC_HS_WI']
	ti = EP_ADRS['ADC_HS_TI']
	wo = EP_ADRS['ADC_HS_WO']
	to = EP_ADRS['ADC_HS_TO']
	#
	# read FIFO flag
	dev.UpdateWireOuts()
	flag = dev.GetWireOutValue(wo)
	#
	fifo_adc0_empty = (flag&(0x00000001<<9 ))>>9
	fifo_adc1_empty = (flag&(0x00000001<<15))>>15
	#
	print('{} = {}'.format('fifo_adc0_empty',fifo_adc0_empty))#
	print('{} = {}'.format('fifo_adc1_empty',fifo_adc1_empty))#
	#
	chk_fifo_adcX_all_empty = \
		fifo_adc0_empty * \
		fifo_adc1_empty;
	#
	print('{} = {}'.format('chk_fifo_adcX_all_empty',chk_fifo_adcX_all_empty))#
	ret = True
	if (chk_fifo_adcX_all_empty!=1):
		print('>>> {}'.format('Warning!: all fifo are not empty.'))
		ret = False
	#
	return ret
#  
#  cmu_adc_is_fifo_non_empty
def cmu_adc_is_fifo_non_empty(): ##
	pass
#  
#  cmu_adc_is_fifo_full
def cmu_adc_is_fifo_full(): ##
	pass
#  
# convert__adc_list__int_float
def convert__adc_list__int_float(adc_list,Sc=1.0):
	adc_list_float = []
	for tt_list in adc_list:
		adc_list_float_temp = [ float(tt)*float(Sc) for tt in tt_list]
		##adc_list_float_temp = []
		##for tt in tt_list:
		##	temp = float(tt)*float(Sc) # scale for voltage
		##	#print(type(temp))
		##	#print(temp)
		##	adc_list_float_temp+=[temp]
		#
		adc_list_float+=[adc_list_float_temp]
	return adc_list_float
#  
#  cmu_adc_display_data_list_int
# FS=10416666  # Hz
# SC=0.00003125 # Volt_1LSB = 8.192V/2^18 = 0.03125 millivolts
def cmu_adc_display_data_list_int (adc_list, FS=10416666):
	# display codes
	print('\n>> {}'.format('Display ADC data'))
	#
	if len(adc_list) < 2:
		print('>>> {}'.format('Warning!: ADC data list must have 2 sub-lists.'))
		return False
	#
	adc0_list_int = adc_list[0]
	adc1_list_int = adc_list[1]
	#
	num_data_to_show = min( 
		len(adc0_list_int),
		len(adc1_list_int))
	print(num_data_to_show)
	#
	plt.ion() # matplotlib interactive mode 
	#
	FIG_NUM = None # for new figure windows
	#FIG_NUM = 1 # for only one figure window
	plt.figure(FIG_NUM,figsize=(12,9))
	#
	# display codes over times
	plt.subplot(311) ### 
	xlist = list(range(num_data_to_show))
	plt.plot(xlist,adc0_list_int[0:num_data_to_show], 'r-')
	plt.plot(xlist,adc1_list_int[0:num_data_to_show], 'b-')
	plt.title('\n\n adc0(red) and adc1(blue)')
	plt.ylabel('Codes')
	plt.xlabel('Samples')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	#
	# display voltage over freq
	#adc_list_float = convert__adc_list__int_float (adc_list, SC)
	NFFT=2**13
	#
	plt.subplot(312) ### 
	_,_,line_psd0 = plt.psd(adc0_list_int, NFFT, FS, noverlap=0, color='r', label='PSD_ADC0', visible=True, alpha=0.7, return_line=True)
	#
	plt.subplot(313) ### 
	_,_,line_psd1 = plt.psd(adc1_list_int, NFFT, FS, noverlap=0, color='b', label='PSD_ADC0', visible=True, alpha=0.7, return_line=True)
	#
	return True
#  
#  cmu_adc_display_data_list_int__zoom
def cmu_adc_display_data_list_int__zoom (adc_list, DUMP_FILENAME='DUMP', FS=10416666, num_smp_zoom=1000):
	# display codes
	print('\n>> {}'.format('Display ADC data (zoomed)'))
	#
	if len(adc_list) < 2:
		print('>>> {}'.format('Warning!: ADC data list must have 2 sub-lists.'))
		return False
	#
	adc0_list_int = adc_list[0]
	adc1_list_int = adc_list[1]
	#
	num_data_to_show = min( 
		len(adc0_list_int),
		len(adc1_list_int))
	#
	# (zoomed)
	num_data_to_show = min(num_data_to_show, num_smp_zoom) 
	#
	print(num_data_to_show)
	#
	##$$plt.ion() # matplotlib interactive mode 
	#
	FIG_NUM = None # for new figure windows
	ff=plt.figure(FIG_NUM,figsize=(12,9))
	#
	# create grid for multicells 
	gs = gridspec.GridSpec(2, 2)
	ax__plt_ = plt.subplot(gs[0, :])
	ax__fft0 = plt.subplot(gs[1, 0])
	ax__fft1 = plt.subplot(gs[1, 1], sharey=ax__fft0)
	#
	# display codes over times (zoomed)
	xlist = list(range(num_data_to_show))
	ax__plt_.plot(xlist,adc0_list_int[0:num_data_to_show], 'r-')
	ax__plt_.plot(xlist,adc1_list_int[0:num_data_to_show], 'b-')
	ax__plt_.set_title('\n\n adc0(red) and adc1(blue)', fontsize=10)
	ax__plt_.set_ylabel('Codes'                       , fontsize=10)
	ax__plt_.set_xlabel('Samples'                     , fontsize=10)
	ax__plt_.grid(True)
	ax__plt_.autoscale(enable=True, axis='x', tight=True)
	#
	#
	# display codes over freq
	# matplotlib.pyplot.psd
	NFFT=2**13
	Pxx_psd0,freqs_psd0,line_psd0 = ax__fft0.psd(adc0_list_int, NFFT, FS, noverlap=0, color='r', label='PSD_ADC0', visible=True, alpha=0.7, return_line=True)
	Pxx_psd1,freqs_psd1,line_psd1 = ax__fft1.psd(adc1_list_int, NFFT, FS, noverlap=0, color='b', label='PSD_ADC0', visible=True, alpha=0.7, return_line=True)
	#
	# draw and show it
	ff.canvas.draw()
	plt.show(block=False)
	#
	# save figure
	if DUMP_FILENAME:
		timestp = datetime.datetime.now().strftime('%Y%m%dT%H%M%S')
		fig_filename  = DUMP_FILENAME+'__{}_{}.png'.format('TS',timestp)
		plt.savefig(fig_filename)
	else:
		fig_filename = ''
	#
	return fig_filename,Pxx_psd0,freqs_psd0,Pxx_psd1,freqs_psd1
#  
#  cmu_adc_display_data_list_int__zoom__full_spectrum
def cmu_adc_display_data_list_int__zoom__full_spectrum(adc_list, DUMP_FILENAME='DUMP', FS=10416666, num_smp_zoom=1000, freq_to_see_in_phase=100000, freq_margin_ratio_to_see_in_phase=0.1):
	# display codes over times, and psd/phase over freq
	print('\n>> {}'.format('Display ADC data (zoomed), and PSD/Phases'))
	#
	if len(adc_list) < 2:
		print('>>> {}'.format('Warning!: ADC data list must have 2 sub-lists.'))
		return False
	#
	adc0_list_int = adc_list[0]
	adc1_list_int = adc_list[1]
	#
	num_data_to_show = min( 
		len(adc0_list_int),
		len(adc1_list_int))
	#
	# (zoomed)
	num_data_to_show = min(num_data_to_show, num_smp_zoom) 
	#
	print(num_data_to_show)
	#
	plt.ion() # matplotlib interactive mode 
	#
	FIG_NUM = None # for new figure windows
	plt.figure(FIG_NUM,figsize=(16,9))
	#
	# create grid for multicells 
	gs = gridspec.GridSpec(2, 3)
	ax__plt_ = plt.subplot(gs[0, :])
	ax__psd0 = plt.subplot(gs[1, 0])
	ax__psd1 = plt.subplot(gs[1, 1], sharey=ax__psd0)
	ax__phs_ = plt.subplot(gs[1, 2])
	#ax__phs0 = plt.subplot(gs[2, 0])
	#ax__phs1 = plt.subplot(gs[2, 1], sharey=ax__phs0)
	#
	# display codes over times (zoomed)
	xlist = list(range(num_data_to_show))
	ax__plt_.plot(xlist,adc0_list_int[0:num_data_to_show], 'r-', alpha=0.7)
	ax__plt_.plot(xlist,adc1_list_int[0:num_data_to_show], 'b-', alpha=0.7)
	ax__plt_.set_title('\n\n adc0(red) and adc1(blue)', fontsize=10)
	ax__plt_.set_ylabel('Codes'                       , fontsize=10)
	ax__plt_.set_xlabel('Samples'                     , fontsize=10)
	ax__plt_.grid(True)
	ax__plt_.autoscale(enable=True, axis='x', tight=True)
	#
	#
	# display psd (power over freq)
	# matplotlib.pyplot.psd
	NFFT=2**13
	Pxx_psd0,freqs_psd0,line_psd0 = ax__psd0.psd(adc0_list_int, NFFT, FS, noverlap=0, color='r', label='PSD_ADC0', visible=True, alpha=0.7, return_line=True)
	Pxx_psd1,freqs_psd1,line_psd1 = ax__psd1.psd(adc1_list_int, NFFT, FS, noverlap=0, color='b', label='PSD_ADC1', visible=True, alpha=0.7, return_line=True)
	#
	S_phs0,freqs_phs0,line_phs0 = ax__phs_.angle_spectrum(adc0_list_int, Fs=FS, color='r', label='PHS_ADC0', visible=True, alpha=0.7)
	S_phs1,freqs_phs1,line_phs1 = ax__phs_.angle_spectrum(adc1_list_int, Fs=FS, color='b', label='PHS_ADC1', visible=True, alpha=0.7)
	#phase_spectrum vs angle_spectrum
	#
	# plot both spectra in one axis 
	#
	#print(freq_to_see_in_phase) #  100000
	#print(freqs_phs0[-1])       # 5208333
	#print(len(freqs_phs0))
	#
	#idx_target = int( (len(freqs_phs0)-1.0)*freq_to_see_in_phase/freqs_phs0[-1] )
	#print(idx_target)
	#print(freqs_phs0[idx_target])
	#
	#start_freq_to_show = int(idx_target*0.8)
	#stop__freq_to_show = min(int(idx_target*1.2), len(freqs_phs0)-1)
	#
	#print(freqs_phs0[start_freq_to_show])
	#print(freqs_phs0[stop__freq_to_show])
	#
	ax_min = freq_to_see_in_phase*(1.0-freq_margin_ratio_to_see_in_phase)
	ax_max = freq_to_see_in_phase*(1.0+freq_margin_ratio_to_see_in_phase)
	ax__phs_.set_xlim([ax_min, ax_max])
	#
	# save figure
	if DUMP_FILENAME:
		timestp = datetime.datetime.now().strftime('%Y%m%dT%H%M%S')
		fig_filename  = DUMP_FILENAME+'__{}_{}.png'.format('TS',timestp)
		plt.savefig(fig_filename)
	else:
		fig_filename = ''
	#
	return fig_filename,Pxx_psd0,freqs_psd0,Pxx_psd1,freqs_psd1,S_phs0,freqs_phs0,S_phs1,freqs_phs1
#  
#  
#  cmu_adc_save_data_list_int_to_csv
def cmu_adc_save_data_list_int_to_csv(adc_list, DUMP_FILENAME='DUMP', FS=10416666): 
	print('\n>> {}'.format('Save ADC data (int) into a file'))
	#
	if not DUMP_FILENAME:
		print('> {}'.format('DUMP filename is required!'))
		return ''
	#
	timestp = datetime.datetime.now().strftime('%Y%m%dT%H%M%S')
	csv_filename  = DUMP_FILENAME+'__{}_{}.csv'.format('TS',timestp)
	#
	print(timestp)
	#
	if len(adc_list) < 2:
		print('>>> {}'.format('Warning!: ADC data list must have 2 sub-lists.'))
		return False
	#
	adc0_list_int = adc_list[0]
	adc1_list_int = adc_list[1]
	#
	len_adc0_list_int = len(adc0_list_int)
	len_adc1_list_int = len(adc1_list_int)
	#
	min_len_adcX = min(len_adc0_list_int,len_adc1_list_int)
	#
	newFile = open(csv_filename, "wt")
	#
	# write header
	# //    Fs [SPS],  Ns [count]
	newFile.write('// {:10s}, {:10s} \n'.format('Fs [SPS]','Ns [count]'))
	newFile.write('// {}, {}\n'.format(FS, min_len_adcX))
	# // 4166666.667,      131072
	# //  adc0,     adc1
	newFile.write('// {:5s}, {:8s}\n'.format('adc0','adc1'))
	#
	# write data 
	for ii in range(0,min_len_adcX):
		newFile.write('{:8d}, {:8d}\n'.format(
			adc0_list_int[ii],
			adc1_list_int[ii])
			)
	newFile.close()
	#
	return csv_filename
#  
#  
#  cmu_adc_save_conf_result_dict_to_file(conf_result_dict)
def cmu_adc_save_conf_result_dict_to_file(conf_result_dict):
	#
	DUMP_FILE_PRE = conf_result_dict['DUMP_FILE_PRE']
	#
	if not DUMP_FILE_PRE:
		print('> {}'.format('DUMP filename is required!'))
		return ''
	#
	timestp = datetime.datetime.now().strftime('%Y%m%dT%H%M%S')
	#
	dict_filename  = DUMP_FILE_PRE+'__{}_{}.dict'.format('TS',timestp)
	#
	# write dict into file
	with open(dict_filename,'w') as f:
		f.write(str(conf_result_dict))
	#
	return dict_filename
#  
#  
#  is_float
def is_float(str):
	try:
		tmp = float(str)
		return True
	except ValueError:
		return False
#  
#  cmu_adc_load_from_csv
def cmu_adc_load_from_csv(DUMP_FILENAME, FS=10416666): 
	#
	# read header and data
	#
	#   // Fs [SPS]  , Ns [count] 
	#   // 8333333, 131072         <-- header
	#   // adc0 , adc1    
	#   -12650,   -44118
	#   ....
	#
	adc0_list = []
	adc1_list = []
	with open(DUMP_FILENAME, 'r', newline='') as csvFile:
		read_data = csv.reader(csvFile, delimiter=',')
		line_count = 0
		for row in read_data:
			line_count += 1
			if row[0][0:2]=='//': # find '//'
				row[0]=row[0][2:] # remove '//'
				flag_float_all = True
				data_header = []
				for cc in row:
					if is_float(cc):
						data_header+=[float(cc)]
					else:
						data_header+=[cc]
						flag_float_all = False
					#
				if flag_float_all==True: # found header and read it
					FS = data_header[0]
					NS = data_header[1]
					continue
				#
			#if line_count>10: break
			else: 
				adc0_list+=[int(row[0])]
				adc1_list+=[int(row[1])]
			#
		#
	# 
	adc_list=[adc0_list,adc1_list]
	return [adc_list,FS,NS]
#  
#  
#  show_data_plot__adc_list
#  show_data_plot_cmu_status__adc_list
#  show_data_time_plot__adc_list
#  show_data_freq_plot__adc_list
#  
#  
####


####
## (11) any functions
#  
#  test batch
#  
####  