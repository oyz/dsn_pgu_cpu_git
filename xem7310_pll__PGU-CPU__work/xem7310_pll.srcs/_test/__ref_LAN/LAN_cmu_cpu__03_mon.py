## ok module for python 3.5
## works both with x64 and win32
## 
##   Windows DLL Usage
##   http://www.opalkelly.com:8090/display/FPSDK/Programming+Languages
##   http://www.microsoft.com/en-us/download/details.aspx?id=40784
##
## https://www.opalkelly.com/examples/home/
## https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel.html
## http://www.opalkelly.com:8090/display/FPSDK/Getting+Started
## wxPython 2.8 http://www.wxpython.org
#
## LAN_cmu_cpu__03_mon.py : test code for FPGA monitoring using LAN interface
#  

####
## controls
#  
#  
FPGA_CONFIGURE = 0
#
BIT_FILENAME = ''; 
#
####

####
## library call
##$$import ok_cmu_cpu__lib as cmu
import LAN_cmu_cpu__lib as cmu
import socket
#
# check configuration : OK_EP_ADRS_CONFIG
#  
EP_ADRS = cmu.conf.OK_EP_ADRS_CONFIG
#  
def form_contents(var,idx):
	return '{} = {}'.format(idx, var[idx])
#
def form_address(var,idx):
	return '{} @ {:#04x}'.format(idx, var[idx])
#  
print(form_contents(EP_ADRS,'board_name'))
print(form_contents(EP_ADRS,'ver'))
print(form_contents(EP_ADRS,'bit_filename'))
#
print(form_address(EP_ADRS,'FPGA_IMAGE_ID'))
#
####

####
## init : dev
dev = cmu.ok_cmu_init()
print(dev)
####

####
ret = cmu.ok_cmu_caller_id()
print(ret)
####

####
## open
#ret = cmu.ok_cmu_open()
#ret = cmu.ok_cmu_open('192.168.168.123',5025)
ips = socket.gethostbyname_ex(socket.gethostname())[-1]
print(ips)
HOST = None
for ip_host in ips:
	if ip_host[0:12]=='192.168.172.':
		HOST = '192.168.172.1'
	if ip_host[0:12]=='192.168.168.':
		HOST = '192.168.168.123'
if HOST==None:
	print('Host should be in the network of 192.168.168.xxx')
	exit()
#
PORT = 5025
#
print('HOST: '+repr(HOST),' PORT: '+repr(PORT))
ret = cmu.ok_cmu_open(HOST,PORT)
print(ret)

####

####
## FPGA_CONFIGURE
if FPGA_CONFIGURE==1: 
	ret = cmu.ok_cmu_conf(BIT_FILENAME)
	print(ret)
####  

####
## read fpga_image_id
fpga_image_id = cmu.cmu_read_fpga_image_id()
#
def form_hex_32b(val):
	return '0x{:08X}'.format(val)
#
print(form_hex_32b(fpga_image_id))
####

####
## read FPGA internal temp and volt
ret = cmu.cmu_monitor_fpga()
print(ret)
####

####
## test counter on 
ret = cmu.cmu_test_counter('ON')
print(ret)
####


####
# work above!
####

####
cmu.sleep(3)
####

####
## test counter off
ret = cmu.cmu_test_counter('OFF')
print(ret)
####

####
## test counter reset
ret = cmu.cmu_test_counter('RESET')
print(ret)
####


####
## close
ret = cmu.ok_cmu_close()
print(ret)
####